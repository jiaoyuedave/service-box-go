package sbox

import (
	"errors"
	"net"
	"os"
	"sync"
	"testing"
	"time"

	"gitee.com/dennis-kk/rpc-go-backend/idlrpc"
	perrors "gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/errors"
	"gitee.com/dennis-kk/service-box-go/common"
	"gitee.com/dennis-kk/service-box-go/example/testcallee"
	"gitee.com/dennis-kk/service-box-go/example/testcaller"
)

type (
	testConn struct{}
	testApp  struct {
		box    *ServiceBox
		cancel chan bool
		wg     sync.WaitGroup
	}
)

func init() {
	err := os.Setenv("BOX_NAME", "env-box-app")
	if err != nil {
		panic(err)
	}
}

func (app *testApp) Init(args []string) error {
	app.cancel = make(chan bool)
	app.wg = sync.WaitGroup{}
	app.box = MakeServiceBox()
	if err := app.box.Init(WithOsArgs(args)); err != nil {
		return err
	}

	return nil
}

func (app *testApp) Start() error {
	if err := app.box.Start(); err != nil {
		return err
	}

	app.wg.Add(1)
	go func() {
		defer app.wg.Done()

		timer := time.NewTimer(10 * time.Millisecond)
		for {
			select {
			case <-timer.C:
				app.box.Tick()
				timer.Reset(10 * time.Millisecond)
			case <-app.cancel:
				return
			}
		}
	}()
	return nil
}

func (app *testApp) Stop() {
	app.cancel <- true
	app.wg.Wait()
	err := app.box.ShutDown()
	if err != nil {
		return
	}
}

func (t *testConn) LocalAddr() (addr net.Addr) {
	return nil
}

func (t *testConn) RemoteAddr() (addr net.Addr) {
	return nil
}

func (t *testConn) Read(b []byte) (n int, err error) {
	return 0, nil
}

func (t *testConn) Write(buf []byte) error {
	return nil
}

func (t *testConn) Close() error {
	return nil
}

func (t *testConn) IsClose() bool {
	return false
}

func (t *testConn) OnClose() {
	return
}

var (
	testChan *BoxChannel
	caller   *idlrpc.PackageInfo
	callee   *idlrpc.PackageInfo
)

func init() {

	caller = &idlrpc.PackageInfo{
		ServiceUUID: testcaller.SrvUUID,
		Creator:     testcaller.InitSDK,
	}

	callee = &idlrpc.PackageInfo{
		ServiceUUID: testcallee.SrvUUID,
		Creator:     testcallee.InitSDK,
	}

	testChan = &BoxChannel{
		conn:   &testConn{},
		status: common.ChannelRunning,
	}
}

// func TestStartWithApollo(t *testing.T) {
// 	app := testApp{}
// 	if err := app.Init([]string{"test_app", "--apollo", "http://106.54.227.205:8080 cloudguan-app plato-service.yaml default ac9179dd8d044605bcd77f0f94ed86f0"}); err != nil {
// 		t.Fatal(err)
// 	}

// 	if err := app.Start(); err != nil {
// 		t.Fatal(err)
// 	}

// 	//try connect to box
// 	conn, err := net.Dial("tcp", "127.0.0.1:7999")
// 	if err != nil {
// 		t.Fatalf("connect to box error %v", err)
// 	}
// 	if conn == nil {
// 		t.Fatalf("test listen error")
// 	}
// 	// 需要让出测试框架的协程给box业务协程调度，在这个用例中是需要的
// 	time.Sleep(100 * time.Millisecond)
// 	conn.Close()

// 	app.Stop()
// }

func TestListen(t *testing.T) {
	app := testApp{}
	if err := app.Init([]string{"test_app", "-c", "default.yaml"}); err != nil {
		t.Fatal(err)
	}

	if err := app.Start(); err != nil {
		t.Fatal(err)
	}

	//try connect to box
	conn, err := net.Dial("tcp", "127.0.0.1:7999")
	if err != nil {
		t.Fatalf("connect to box error %v", err)
	}
	if conn == nil {
		t.Fatalf("test listen error")
	}
	// 需要让出测试框架的协程给box业务协程调度，在这个用例中是需要的
	time.Sleep(100 * time.Millisecond)
	conn.Close()

	app.Stop()
}

func TestConnectDirect(t *testing.T) {
	//启动监听app
	appListen := testApp{}
	if err := appListen.Init([]string{"app_listen", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}
	if err := appListen.Start(); err != nil {
		t.Fatal(err)
	}

	//启动直连
	app := testApp{}
	if err := app.Init([]string{"test_app", "-c", "test_direct_connect.yaml"}); err != nil {
		t.Fatal(err)
	}
	if err := app.Start(); err != nil {
		t.Fatal(err)
	}
	time.Sleep(time.Millisecond * 100)
	if app.box.directHandle == nil {
		t.Fatal("test connect direct error")
	}

	appListen.Stop()
	app.Stop()
}

func TestConnectDirectGetProxy(t *testing.T) {
	//启动监听app
	appListen := testApp{}
	if err := appListen.Init([]string{"app_listen", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}

	if err := appListen.Start(); err != nil {
		t.Fatal(err)
	}

	//启动直连
	app := testApp{}
	if err := app.Init([]string{"test_app", "-c", "test_direct_connect.yaml"}); err != nil {
		t.Fatal(err)
	}

	if err := app.box.AddServicePackage(callee); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := app.Start(); err != nil {
		t.Fatal(err)
	}
	time.Sleep(time.Millisecond * 100)

	proxy, err := app.box.GetProxy(callee.ServiceUUID)
	if err != nil || proxy == nil {
		t.Fatalf("get callee error %v", err)
	}

	appListen.Stop()
	app.Stop()
}

func TestConnectDirectCallMethod(t *testing.T) {
	//启动监听app
	appListen := testApp{}
	if err := appListen.Init([]string{"app_listen", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}

	if err := appListen.box.AddServicePackage(callee); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := appListen.Start(); err != nil {
		t.Fatal(err)
	}

	//启动直连
	app := testApp{}
	if err := app.Init([]string{"test_app", "-c", "test_direct_connect.yaml"}); err != nil {
		t.Fatal(err)
	}

	if err := app.box.AddServicePackage(callee); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := app.Start(); err != nil {
		t.Fatal(err)
	}
	time.Sleep(time.Millisecond * 100)

	proxy, err := app.box.GetProxy(callee.ServiceUUID)
	if err != nil || proxy == nil {
		t.Fatalf("get callee error %v", err)
	}

	pCallee, ok := proxy.(*testcallee.TestCalleeProxy)
	if !ok {
		t.Fatalf("not TestCalleeProxy proxy")
	}

	res, err := pCallee.Add(1, 2)
	if err != nil || res != 3 {
		t.Error("Callee Add Failed")
	}

	appListen.Stop()
	app.Stop()
}

func TestAddService(t *testing.T) {
	app := testApp{}
	if err := app.Init([]string{"test_app", "-c", "default.yaml"}); err != nil {
		t.Fatal(err)
	}

	if err := app.box.AddServicePackage(callee); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := app.box.AddServicePackage(caller); err != nil {
		t.Fatalf("Add caller error %v", err)
	}

	if err := app.Start(); err != nil {
		t.Fatalf("start box error %v", err)
	}

	proxy, err := app.box.rpc.GetServiceProxy(callee.ServiceUUID, testChan)
	if err != nil {
		t.Fatalf("get callee error %v", err)
	}

	if proxy == nil {
		t.Fatalf("test add service %d failed", callee.ServiceUUID)
	}

	proxy, err = app.box.rpc.GetServiceProxy(caller.ServiceUUID, testChan)
	if err != nil {
		t.Fatalf("get callee error %v", err)
	}

	if proxy == nil {
		t.Fatalf("test add service %d failed", caller.ServiceUUID)
	}

	app.Stop()
}

func TestMultiGetProxy(t *testing.T) {
	app := testApp{}
	if err := app.Init([]string{"test_app", "-c", "default.yaml"}); err != nil {
		t.Fatal(err)
	}

	if err := app.box.AddServicePackage(callee); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := app.box.AddServicePackage(caller); err != nil {
		t.Fatalf("Add caller error %v", err)
	}

	if err := app.Start(); err != nil {
		t.Fatalf("start box error %v", err)
	}
	notify := make(chan error)
	getf := func() {
		proxy, err := app.box.rpc.GetServiceProxy(callee.ServiceUUID, testChan)
		if proxy == nil {
			err = errors.New("get proxy failed! ")
		}
		notify <- err
	}

	for i := 0; i < 5; i++ {
		go getf()
	}

	for i := 0; i < 5; i++ {
		err := <-notify
		if err != nil {
			t.Error(err)
		}
	}
	app.Stop()
}

func TestFindService(t *testing.T) {
	app := testApp{}
	if err := app.Init([]string{"test_app", "-c", "default.yaml"}); err != nil {
		t.Fatal(err)
	}

	if err := app.box.AddServicePackage(callee); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := app.Start(); err != nil {
		t.Fatalf("start box error %v", err)
	}

	//find
	proxy, err := app.box.GetProxyWithNickName(callee.ServiceUUID, "test_case_callee")
	if err != nil {
		t.Fatalf("get proxy error %v", err)
	}

	if proxy == nil {
		t.Fatalf("test find service error ")
	}

	app.Stop()
}

func TestMultiFind(t *testing.T) {
	app := testApp{}
	if err := app.Init([]string{"test_app", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}

	if err := app.box.AddServicePackage(callee); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := app.Start(); err != nil {
		t.Fatalf("start box error %v", err)
	}

	multi := make(chan error, 2)
	gf := func() {
		proxy, err := app.box.GetProxy(callee.ServiceUUID)
		if proxy == nil {
			err = errors.New("Get proxy failed! ")
		}
		multi <- err
	}
	for i := 0; i < 10; i++ {
		go gf()
	}
	//协程里自带超时
	for i := 0; i < 10; i++ {
		select {
		case err := <-multi:
			if err != nil {
				t.Fatal(err)
			}
		}
	}
	app.Stop()
}

func TestCallMethod(t *testing.T) {
	app1 := testApp{}
	if err := app1.Init([]string{"test_app", "-c", "default.yaml"}); err != nil {
		t.Fatal(err)
	}
	app2 := testApp{}
	if err := app2.Init([]string{"call_app", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}

	if err := app1.box.AddServicePackage(callee); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := app1.Start(); err != nil {
		t.Fatalf("start app1 error %v", err)
	}

	if err := app2.box.AddServicePackage(callee); err != nil {
		t.Fatalf("app2 add callee error %v", err)
	}

	if err := app2.Start(); err != nil {
		t.Fatalf("start app2 error %v", err)
	}

	proxy, err := app2.box.GetProxyWithNickName(callee.ServiceUUID, "test_case_callee")
	if err != nil || proxy == nil {
		t.Fatalf("get proxy %d failed %v", callee.ServiceUUID, err)
	}

	pCallee, ok := proxy.(*testcallee.TestCalleeProxy)
	if !ok {
		t.Fatalf("not TestCalleeProxy proxy")
	}

	res, err := pCallee.Add(1, 2)
	if err != nil || res != 3 {
		t.Error("Callee Add Failed")
	}

	res, err = pCallee.Sub(3, 1)
	if err != nil || res != 2 {
		t.Error("Callee Sub Failed")
	}

	app1.Stop()
	app2.Stop()
}

func TestCallMethodWithReturnError(t *testing.T) {
	app1 := testApp{}
	if err := app1.Init([]string{"test_app", "-c", "default.yaml"}); err != nil {
		t.Fatal(err)
	}
	app2 := testApp{}
	if err := app2.Init([]string{"call_app", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}

	if err := app1.box.AddServicePackage(callee); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := app1.Start(); err != nil {
		t.Fatalf("start app1 error %v", err)
	}

	if err := app2.box.AddServicePackage(callee); err != nil {
		t.Fatalf("app2 add callee error %v", err)
	}

	if err := app2.Start(); err != nil {
		t.Fatalf("start app2 error %v", err)
	}

	proxy, err := app2.box.GetProxyWithNickName(callee.ServiceUUID, "test_case_callee")
	if err != nil || proxy == nil {
		t.Fatalf("get proxy %d failed %v", callee.ServiceUUID, err)
	}

	pCallee, ok := proxy.(*testcallee.TestCalleeProxy)
	if !ok {
		t.Fatalf("not TestCalleeProxy proxy")
	}

	_, err = pCallee.Sub(1, 3)
	if err != perrors.ErrRpcException {
		t.Errorf("test return error failed, error is %v", err)
	}

	app1.Stop()
	app2.Stop()
}
