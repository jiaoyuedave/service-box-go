// Package box Cross platform box function
package sbox

func (sb *ServiceBox) loadAllService() error {
	// load service from waitload pack aage
	for n, p := range sb.waitLoadPack {
		err := sb.loadServiceByPackInfo(p)
		if err != nil {
			sb.logger.Warn("[ServiceBox] load service %d failed %v", err, n)
			continue
		}
	}
	//clean package cache
	sb.waitLoadPack = make(packCache)
	return nil
}
