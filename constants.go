package sbox

type (
	state uint32 //service box state type
)

const (
	stateClosed state = iota
	stateInited
	stateRunning
)
