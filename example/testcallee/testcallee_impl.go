// Generated by the go idl tools. DO NOT EDIT 2022-03-15 23:05:32
// source: box_example
package testcallee

import "context"
import "errors"

type TestCalleeImpl struct {
}

func NewTestCallee() *TestCalleeImpl {
	service := &TestCalleeImpl{}
	return service
}

func (sp *TestCalleeImpl) GetUUID() uint64 {
	return 6784101180412288340
}

func (sp *TestCalleeImpl) GetNickName() string {
	return "6784101180412288340"
}

func (sp *TestCalleeImpl) OnAfterFork(ctx context.Context) bool {
	//add this service to framework, do not call rpc method in this function
	return true
}

func (sp *TestCalleeImpl) OnTick() bool {
	//tick function in main goroutine
	return true
}

func (sp *TestCalleeImpl) OnBeforeDestroy() bool {
	//tick function in main goroutine
	return true
}
func (sp *TestCalleeImpl) Add(ctx context.Context, _1 int32, _2 int32) (ret1 int32, err error) {
	ret1 = _1 + _2
	return
}

func (sp *TestCalleeImpl) Sub(ctx context.Context, _1 int32, _2 int32) (ret1 int32, err error) {
	if _1 < _2 {
		err = errors.New("test error return")
	} else {
		ret1 = _1 - _2
	}
	return
}
