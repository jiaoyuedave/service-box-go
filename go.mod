module gitee.com/dennis-kk/service-box-go

go 1.16

//gitee.com/dennis-kk/rpc-go-backend v0.3.0
//github.com/panjf2000/gnet v1.6.4

require (
	gitee.com/dennis-kk/rpc-go-backend v0.4.13
	github.com/BurntSushi/toml v0.3.1
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gin-gonic/gin v1.7.7
	github.com/go-redis/redis/v8 v8.11.5
	github.com/go-zookeeper/zk v1.0.2
	github.com/golang-jwt/jwt/v4 v4.4.3
	github.com/golang/protobuf v1.5.2
	github.com/imdario/mergo v0.3.12
	github.com/jonboulle/clockwork v0.2.2 // indirect
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.5 // indirect
	github.com/panjf2000/gnet v1.6.4
	github.com/urfave/cli/v2 v2.3.0
	go.mongodb.org/mongo-driver v1.9.1
	go.uber.org/zap v1.19.1
	google.golang.org/protobuf v1.26.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
