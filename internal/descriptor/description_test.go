package descriptor

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"
	"time"

	"gitee.com/dennis-kk/service-box-go/util/slog"
	"gitee.com/dennis-kk/service-box-go/util/slog/zap"
)

const (
	testEffectiveJson = `
{
    "idlname" : "ping",
    "serviceNames" :
    [
        "Ping"
    ],
    "services" :
    [
        {
            "loadType" : "dynamic",
            "methods" :
            [
                {
                    "arguments" :
                    [
                        {
                            "IdlType" : "ui64",
                            "decl_name" : "sndts",
                            "index" : 1,
                            "type" : "uint64"
                        }
                    ],
                    "index" : 1,
                    "is_stream" : false,
                    "name" : "ping",
                    "noexcept" : false,
                    "public" : true,
                    "retType" :
                    {
                        "IdlType" : "struct",
                        "isStruct" : true,
                        "type" : "PingReply"
                    },
                    "timeout" : 5000
                }
            ],
            "name" : "Ping",
            "type" : "single",
            "uuid" : "1089214916311906258"
        }
    ]
}
`
)

var (
	logger          slog.BoxLogger
	ss              []*ServiceDescriptor
	testServiceName string
)

func init() {
	logger, _ = zap.NewLogger(slog.WithCallerSkipCount(2))
}

func testWatchHandle(changeType int, serivices []*ServiceDescriptor) error {
	for _, s := range serivices {
		testServiceName = s.Name
		for _, m := range s.Methods {
			testServiceName += ":" + m.Name
		}
	}

	ss = serivices
	return nil
}

func testMakeDataBase() (*DataBase, error) {
	db := MakeDataBase()
	err := db.Init(WithLogger(logger), WithConfigPath("../../example/idl"), WithConfigSuffix(".json"))
	return db, err
}

func TestDataBase_Init(t *testing.T) {
	db, err := testMakeDataBase()
	if err != nil {
		t.Fatalf("new database error %v", err)
	}

	err = db.Start()
	if err != nil {
		t.Fatalf("new database error %v", err)
	}

	db.ShutDown()
}

func TestDataBase_AddWatcher(t *testing.T) {
	db, err := testMakeDataBase()
	if err != nil {
		t.Fatalf("new database error %v", err)
	}

	err = db.Start()
	if err != nil {
		t.Fatalf("new database error %v", err)
	}

	db.AddWatcher(testWatchHandle)
	//write file to example
	file, err := os.OpenFile("../../example/idl/ping.json", os.O_WRONLY|os.O_CREATE, 0765)
	if err != nil {
		t.Fatalf("write file error %v", err)
	}

	file.Write([]byte(testEffectiveJson))
	file.Close()

	time.Sleep(100 * time.Millisecond)

	if testServiceName != "Ping:ping" {
		t.Fatalf("unexcepted new service %q", testServiceName)
	}

	db.ShutDown()

	os.Remove("../../example/idl/ping.json")
}

// TestDataBase_Start 测试服务是否被正常加载
func TestDataBase_FindFile(t *testing.T) {
	db, err := testMakeDataBase()
	if err != nil {
		t.Fatalf("new database error %v", err)
	}

	err = db.Start()
	if err != nil {
		t.Fatalf("new database error %v", err)
	}

	// 检测服务是否被正确获取到
	services := db.FindFile("testcallee.idl.protobuf.json")
	if services == nil {
		t.Fatalf("service testcallee.idl.protobuf.json  load failed ")
	}

	// 检查服务是否正确加载
	if services.IdlName != "testcallee" {
		t.Fatalf("unexcepted idl name %q ", services.IdlName)
	}

	// 检查方法是否加载正确
	if len(services.Services[0].Methods) != 2 {
		t.Fatal("un corrected service method len !")
	}

	db.ShutDown()
}

// TestDataBase_ModifyFile 测试文件修改是否能正确监听到
func TestDataBase_ModifyFile(t *testing.T) {
	db, err := testMakeDataBase()
	if err != nil {
		t.Fatalf("new database error %v", err)
	}

	err = db.Start()
	if err != nil {
		t.Fatalf("new database error %v", err)
	}

	db.AddWatcher(testWatchHandle)

	// 检测服务是否被正确获取到
	services := db.FindFile("testcallee.idl.protobuf.json")
	if services == nil {
		t.Fatalf("service testcallee.idl.protobuf.json  load failed ")
	}

	// 检查服务是否正确加载
	if services.IdlName != "testcallee" {
		t.Fatalf("unexcepted idl name %q ", services.IdlName)
	}

	services.Services[0].Name = "TestPlato"

	// 先读取数据缓存下来
	originJson, _ := ioutil.ReadFile("../../example/idl/testcallee.idl.protobuf.json")

	// 写入文件
	file, err := os.OpenFile("../../example/idl/testcallee.idl.protobuf.json", os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0765)
	if err != nil {
		t.Fatalf("write file error %v", err)
	}

	bytes, _ := json.Marshal(services)

	file.Write(bytes)
	file.Close()

	time.Sleep(50 * time.Millisecond)

	if ss == nil {
		t.Fatalf("no watcher method recved !")
	}

	if len(ss) != 1 {
		t.Fatalf("unexcepted services loaded !")
	}

	if ss[0].Name != "TestPlato" {
		t.Fatalf("unexcepted service name %s", ss[0].Name)
	}

	db.ShutDown()

	// 写入文件
	file, err = os.OpenFile("../../example/idl/testcallee.idl.protobuf.json", os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0765)
	if err != nil {
		t.Fatalf("write file error %v", err)
	}
	file.Write(originJson)
	file.Close()
}

// TestDataBase_AddNewDir
func TestDataBase_AddNewDir(t *testing.T) {
	db, err := testMakeDataBase()
	if err != nil {
		t.Fatalf("new database error %v", err)
	}

	ss = nil

	err = db.Start()
	if err != nil {
		t.Fatalf("new database error %v", err)
	}

	db.AddWatcher(testWatchHandle)

	// 复制文件见到idl目录
	err = os.Rename("../../example/sub", "../../example/idl/sub")
	if err != nil {
		t.Fatalf("rename floder error %s", err.Error())
	}

	// 让主循环tick
	time.Sleep(50 * time.Millisecond)

	if len(ss) != 2 {
		t.Fatalf("unexcepted service's length")
	}

	if ss[0].Name != "TestCase1" {
		t.Fatalf("unexcepted service's name %s ", ss[0].Name)
	}

	if ss[1].Name != "TestCase2" {
		t.Fatalf("unexcepted service's name %s ", ss[0].Name)
	}

	db.ShutDown()

	err = os.Rename("../../example/idl/sub", "../../example/sub")
	if err != nil {
		t.Fatalf("rename floder error %s", err.Error())
	}
}
