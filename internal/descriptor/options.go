package descriptor

import "gitee.com/dennis-kk/service-box-go/util/slog"

type (
	//Options 描述符
	Options struct {
		DescriptionPath string         `json:"description_path"` //配置路径
		Suffix          string         `json:"suffix"`           //配置文件后缀
		logger          slog.BoxLogger //日志接口
	}
	Option func(option *Options)
)

func WithConfigSuffix(suffix string) Option {
	return func(option *Options) {
		option.Suffix = suffix
	}
}

func WithConfigPath(path string) Option {
	return func(option *Options) {
		option.DescriptionPath = path
	}
}

func WithLogger(logger slog.BoxLogger) Option {
	return func(option *Options) {
		option.logger = logger
	}
}
