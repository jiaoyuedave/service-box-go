package jsonpb

import "sync"

type (
	//JsonPbFactoryFunc 工厂函数原型
	JsonPbFactoryFunc func(...Option) (IJsonPbCoder, error)
)

var (
	//coderFactory coder 模板工厂
	coderFactory map[string]JsonPbFactoryFunc
	once         sync.Once
)

func init() {
	once.Do(func() {
		coderFactory = make(map[string]JsonPbFactoryFunc)
	})
}

//MakeJsonPbWithType 根据类型创建jsonpb转码器
func MakeJsonPbWithType(name string, opts ...Option) (IJsonPbCoder, error) {
	creator, ok := coderFactory[name]
	if !ok {
		return nil, UnSupportCoderType
	}
	return creator(opts...)
}

//RegisterToFactory 注册JsonPbCoder的类型
func RegisterToFactory(name string, factoryFunc JsonPbFactoryFunc) {
	coderFactory[name] = factoryFunc
}
