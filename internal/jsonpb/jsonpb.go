package jsonpb

import "errors"

var (
	UnSupportCoderType = errors.New("unkown jsonpn coder type")
	InvalidConfigPath  = errors.New("invalid service config path")
)

type (
	//IJsonPbCoder json string to pb 的编码器
	IJsonPbCoder interface {
		//Init 初始化coder 模块
		Init(...Option) error
		//Restart 重启整个模块，会重新加载配置
		Restart() error
		//JsonStrToProtoBuffer json 请求字符串转化为pb 序列化字节流
		JsonStrToProtoBuffer(string, string, string) ([]byte, error)
		//ProtoBufferToJson pb 字节流 转化为 json 格式
		ProtoBufferToJson(string, string, []byte) (string, error)
		//UnInit 卸载coder 模块
		UnInit() error
	}
)
