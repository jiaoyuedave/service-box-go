//+build jsonpbcpp

package jsonpbcpp

import (
	"gitee.com/dennis-kk/service-box-go/internal/jsonpb"
	"sync"
)

var (
	once sync.Once
)

func init() {
	once.Do(func() {
		//注册cpp实现的json
		jsonpb.RegisterToFactory("jsonpbcpp", MakeJsonPbCoderCpp)
	})
}

func MakeJsonPbCoderCpp(opts ...jsonpb.Option) (jsonpb.IJsonPbCoder, error) {
	cppCoder := &JsonPbCpp{
		options: &jsonpb.Options{},
	}

	if err := cppCoder.Init(opts...); err != nil {
		return nil, err
	}

	return cppCoder, nil
}
