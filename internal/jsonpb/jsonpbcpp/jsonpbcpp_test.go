//+build jsonpbcpp

package jsonpbcpp

import (
	"encoding/json"
	"gitee.com/dennis-kk/service-box-go/example/idldata/pbdata"
	"gitee.com/dennis-kk/service-box-go/internal/jsonpb"
	"github.com/golang/protobuf/proto"
	"testing"
)

//TestMakeJsonPbCoderCpp 创建编码器
func TestMakeJsonPbCoderCpp(t *testing.T) {
	coder, err := MakeJsonPbCoderCpp(jsonpb.WithConfigPath("../../../example/idl"))
	if err != nil {
		t.Fatalf("create cpp json coder error %v", err)
	}

	if coder == nil {
		t.Fatal("invalid coder instance !")
	}

	if err := coder.UnInit(); err != nil {
		t.Fatal(err)
	}
}

//TestJsonPbCpp_JsonStrToProtoBuffer
func TestJsonPbCpp_JsonStrToProtoBuffer(t *testing.T) {
	coder, err := MakeJsonPbCoderCpp(jsonpb.WithConfigPath("../../../example/idl"))
	if err != nil {
		t.Fatalf("create cpp json coder error %v", err)
	}

	if coder == nil {
		t.Fatal("invalid coder instance !")
	}

	jsonArgs := `{
	"args":{
		"arg1": "plato service",
	}
	}`
	// 编码
	buffer, err := coder.JsonStrToProtoBuffer("TestCaller", "SetInfo", jsonArgs)
	if err != nil {
		t.Fatalf("code json args error %v ", err)
	}

	// 测试pb 能否正确反序列化
	pbMsg := pbdata.TestCaller_SetInfoArgs{}

	err = proto.Unmarshal(buffer, &pbMsg)
	if err != nil {
		t.Fatalf("code pb message error %v ", err)
	}

	if pbMsg.Arg1 != "plato service" {
		t.Fatalf("unexcepted Unmarshal str %s ", pbMsg.Arg1)
	}

	if err := coder.UnInit(); err != nil {
		t.Fatal(err)
	}
}

func TestJsonPbCpp_ProtoBufferToJson(t *testing.T) {
	coder, err := MakeJsonPbCoderCpp(jsonpb.WithConfigPath("../../../example/idl"))
	if err != nil {
		t.Fatalf("create cpp json coder error %v", err)
	}

	if coder == nil {
		t.Fatal("invalid coder instance !")
	}

	// 测试pb 测试转换json是否正确
	pbMsg := pbdata.TestCaller_GetInfoRet{
		Ret1: "plato test",
	}
	// 序列化
	buffer, err := proto.Marshal(&pbMsg)
	if err != nil {
		t.Fatalf("marshal proto message error %v", err)
	}

	jsonStr, err := coder.ProtoBufferToJson("TestCaller", "GetInfo", buffer)
	if err != nil {
		t.Fatalf("convert pbbuffer to json error  %v", err)
	}

	resOb := struct {
		Ret1 string `json:"ret1"`
	}{}

	err = json.Unmarshal([]byte(jsonStr[:]), &resOb)
	if err != nil {
		t.Fatalf("code json error  %v", err)
	}

	if resOb.Ret1 != "plato test" {
		t.Fatalf("unexcepted Unmarshal str %s ", resOb.Ret1)
	}

	if err := coder.UnInit(); err != nil {
		t.Fatal(err)
	}
}

// 不正确的pb字段能否正确序列化
func TestJsonPbCpp_ProtoBufferToJsonWithErrorBuffer(t *testing.T) {
	coder, err := MakeJsonPbCoderCpp(jsonpb.WithConfigPath("../../../example/idl"))
	if err != nil {
		t.Fatalf("create cpp json coder error %v", err)
	}

	if coder == nil {
		t.Fatal("invalid coder instance !")
	}

	// 测试pb 测试转换json是否正确
	pbMsg := pbdata.TestCaller_SetInfoArgs{
		Arg1: "plato go",
	}

	// 序列化
	buffer, err := proto.Marshal(&pbMsg)
	if err != nil {
		t.Fatalf("marshal proto message error %v", err)
	}

	jsonStr, err := coder.ProtoBufferToJson("TestCallee", "Add", buffer[:6])
	if err == nil {
		t.Fatalf("un coorrect error check for wrong pb buffer %s", jsonStr)
	}

	if len(jsonStr) != 0 {
		t.Fatalf("unexceptoion json string %s", jsonStr)
	}

	t.Log(err)

	if err := coder.UnInit(); err != nil {
		t.Fatal(err)
	}
}

func BenchmarkJsonPbCpp_JsonStrToProtoBuffer(b *testing.B) {
	coder, err := MakeJsonPbCoderCpp(jsonpb.WithConfigPath("../../../example/idl"))
	if err != nil {
		b.Fatalf("create cpp json coder error %v", err)
	}

	if coder == nil {
		b.Fatal("invalid coder instance !")
	}

	jsonArgs := `{
	"args":{
		"arg1": "plato service",
	}
	}`

	for n := 0; n < b.N; n++ {
		// 编码
		_, err := coder.JsonStrToProtoBuffer("TestCaller", "SetInfo", jsonArgs)
		if err != nil {
			b.Fatalf("code json args error %v ", err)
		}
	}

	if err := coder.UnInit(); err != nil {
		b.Fatal(err)
	}
}

func BenchmarkJsonPbCpp_ProtoBufferToJson(b *testing.B) {
	coder, err := MakeJsonPbCoderCpp(jsonpb.WithConfigPath("../../../example/idl"))
	if err != nil {
		b.Fatalf("create cpp json coder error %v", err)
	}

	if coder == nil {
		b.Fatal("invalid coder instance !")
	}

	// 测试pb 测试转换json是否正确
	pbMsg := pbdata.TestCaller_GetInfoRet{
		Ret1: "plato test",
	}
	// 序列化
	buffer, err := proto.Marshal(&pbMsg)
	if err != nil {
		b.Fatalf("marshal proto message error %v", err)
	}

	for n := 0; n < b.N; n++ {
		_, err := coder.ProtoBufferToJson("TestCaller", "GetInfo", buffer)
		if err != nil {
			b.Fatalf("convert pbbuffer to json error  %v", err)
		}
	}
}
