//+build jsonpbcpp,gcc8

package jsonpbcpp

// #cgo LDFLAGS: -L${SRCDIR}/../lib/linux/gcc8 -ljson_pb -ldl -Wl,-rpath,${SRCDIR}/../lib/linux/gcc8
// #cgo CFLAGS: -I${SRCDIR}/../include
import "C"
