//+build jsonpbcpp,gcc9

package jsonpbcpp

// #cgo LDFLAGS: -L${SRCDIR}/../lib/linux/gcc9 -ljson_pb -ldl -Wl,-rpath,${SRCDIR}/../lib/linux/gcc9
// #cgo CFLAGS: -I${SRCDIR}/../include
import "C"
