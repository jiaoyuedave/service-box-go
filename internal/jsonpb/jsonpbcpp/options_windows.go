//+build jsonpbcpp,windows

package jsonpbcpp

// #cgo LDFLAGS: -L${SRCDIR}/../lib/windows -Wl,-rpath,${SRCDIR}/../lib/windows -ljson_pb
// #cgo CFLAGS: -I${SRCDIR}/../include
import "C"
