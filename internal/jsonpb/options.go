package jsonpb

type (
	//Option 设置函数
	Option func(options *Options)

	//Options 模块配置
	Options struct {
		//CfgPath 加载配置路径
		CfgPath string `yaml:"cfg_path"`
	}
)

func WithConfigPath(path string) Option {
	return func(option *Options) {
		option.CfgPath = path
	}
}
