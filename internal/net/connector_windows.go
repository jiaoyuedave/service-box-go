package net

type winConnector struct {
}

func (c *winConnector) Start() error {
	if c == nil {
		return nil
	}
	return nil
}

func (c *winConnector) ConnectTo(network, address string) (IBoxConn, error) {
	if c == nil {
		return nil, nil
	}
	return nil, nil
}

func (c *winConnector) Stop() error {
	if c == nil {
		return nil
	}
	return nil
}

func makeConnector(v interface{}) IBoxConnector {
	return &winConnector{}
}
