package net

import "net"

// NetEventQueue event channel type
type NetEventQueue chan *EventData

//IBoxConn box net connection wrapper !
type IBoxConn interface {
	// LocalAddr is the connection's local socket address.
	LocalAddr() (addr net.Addr)
	// RemoteAddr is the connection's remote peer address.
	RemoteAddr() (addr net.Addr)

	// Read reads data from the connection.
	Read(b []byte) (n int, err error)

	// Write writes one byte slice to peer asynchronously, usually you would call it in individual goroutines
	Write(buf []byte) error

	// Close closes the current connection.
	Close() error

	// IsClose will return ture if connection has been closed
	IsClose() bool

	OnClose()
}

//IBoxConnector connector wrapper
type IBoxConnector interface {
	//Start start connector logic
	Start() error
	//ConnectTo connect to address
	ConnectTo(network, address string) (IBoxConn, error)
	//Stop stop all connect
	Stop() error
}

//IBoxNetLoop box network loop
type IBoxNetLoop interface {
	//Start create network loop
	Start() error
	//Stop stop all nerwork loop
	Stop() error
	//Tick network goroutine tick func
	Tick()
	//Notify notify logic goroutine net event
	Notify(data *EventData)
	//SendToLoop send event to network goroutine, deal by tick function
	SendToLoop(data *EventData)
	//ConnectTo connect to remote server
	ConnectTo(network, address string) (IBoxConn, error)
	//ListenAt listen at address
	ListenAt(network, address string) error
}

// BoxConn create new box conn, param: gnet.Conn
var BoxConn = makeGnetConn

// BoxConnector client connector, param: serverEH
var BoxConnector = makeConnector

// BoxLoop box network loop, listen or connect, param: NetEventQueue
var BoxLoop = makeGnetLoop
