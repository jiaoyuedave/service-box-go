package net

import (
	"gitee.com/dennis-kk/service-box-go/common"
	"net"
	"sync/atomic"
	"testing"
	"time"
)

var (
	testLoop   *gnetLoop
	testNotify NetEventQueue
)

func TestListen(t *testing.T) {
	testNotify = make(NetEventQueue, 64)
	testLoop = makeGnetLoop(testNotify).(*gnetLoop)

	err := testLoop.Start()
	if err != nil {
		t.Fatal("start gnet loop")
	}

	if err := testLoop.ListenAt("tcp", "127.0.0.1:4666"); err != nil {
		t.Fatalf("listen at host error %v", err)
	}
	time.Sleep(50 * time.Millisecond)

	if atomic.LoadInt32(&testLoop.sEH.status) != common.NetworkRunning {
		t.Error("listen at host error")
	}

	if err := testLoop.Stop(); err != nil {
		t.Fatal("stop gnet loop")
	}
}

func TestGnetConnect(t *testing.T) {
	testNotify = make(NetEventQueue, 64)
	testLoop = makeGnetLoop(testNotify).(*gnetLoop)

	err := testLoop.Start()
	if err != nil {
		t.Fatal("start gnet loop")
	}

	if err := testLoop.ListenAt("tcp", "127.0.0.1:2333"); err != nil {
		t.Fatalf("listen at host error %v", err)
	}

	time.Sleep(time.Millisecond * 100)

	conn, err := net.Dial("tcp", "127.0.0.1:2333")
	if err != nil {
		t.Fatalf("connect at host error %v", err)
	}

	event := <-testNotify
	if event.NetType != EventAccept {
		t.Fatal(" gnet loop accept error")
	}

	if err = conn.Close(); err != nil {
		t.Fatalf("connect close %v", err)
	}

	event = <-testNotify
	if event.NetType != EventClose {
		t.Fatal(" gnet loop close error")
	}

	if err = testLoop.Stop(); err != nil {
		t.Fatal("stop gnet loop")
	}
}

func TestRecvMessage(t *testing.T) {
	testNotify = make(NetEventQueue, 64)
	testLoop = makeGnetLoop(testNotify).(*gnetLoop)

	err := testLoop.Start()
	if err != nil {
		t.Fatal("start gnet loop")
	}

	if err := testLoop.ListenAt("tcp", "127.0.0.1:2333"); err != nil {
		t.Fatalf("listen at host error %v", err)
	}

	time.Sleep(time.Millisecond * 100)
	conn, err := net.Dial("tcp", "127.0.0.1:2333")
	if err != nil {
		t.Fatalf("connect at host error %v", err)
	}

	event := <-testNotify
	if event.NetType != EventAccept {
		t.Fatal(" gnet loop accept error")
	}

	_, err = conn.Write([]byte("cloudguan"))
	if err != nil {
		t.Fatal(" connect send error")
	}

	event = <-testNotify
	if event.NetType != EventReceive {
		t.Fatal(" gnet recv error")
	}

	rEvent := event.Data.(*ReceiveEvent)
	if string(rEvent.Data) != "cloudguan" {
		t.Fatal(" gnet recv error")
	}
	if err = conn.Close(); err != nil {
		t.Fatalf("connect close %v", err)
	}

	event = <-testNotify
	if event.NetType != EventClose {
		t.Fatal(" gnet loop close error")
	}

	if err = testLoop.Stop(); err != nil {
		t.Fatal("stop gnet loop")
	}
}

//TestConnAndCloseImmediately 连接之后立刻断开
func TestConnAndCloseImmediately(t *testing.T) {
	testNotify = make(NetEventQueue, 64)
	testLoop = makeGnetLoop(testNotify).(*gnetLoop)

	err := testLoop.Start()
	if err != nil {
		t.Fatal("start gnet loop")
	}

	if err := testLoop.ListenAt("tcp", "127.0.0.1:2333"); err != nil {
		t.Fatalf("listen at host error %v", err)
	}

	time.Sleep(time.Millisecond * 100)

	conn, err := net.Dial("tcp", "127.0.0.1:2333")
	if err != nil {
		t.Fatalf("connect at host error %v", err)
	}

	err = conn.Close()
	if err != nil {
		t.Fatalf("connect at host error %v", err)
	}

	time.Sleep(time.Millisecond * 100)

	event := <-testNotify
	cEvent, ok := event.Data.(*AcceptEvent)
	if !ok {
		t.Fatal("error network event type !")
	}

	if cEvent.Conn.IsClose() != true {
		t.Fatal("error network event status  !")
	}

	if err = testLoop.Stop(); err != nil {
		t.Fatal("stop gnet loop")
	}

}

func TestMultiClientConnect(t *testing.T) {
	testNotify = make(NetEventQueue, 64)
	testLoop = makeGnetLoop(testNotify).(*gnetLoop)

	err := testLoop.Start()
	if err != nil {
		t.Fatal("start gnet loop")
	}

	if err := testLoop.ListenAt("tcp", "127.0.0.1:2333"); err != nil {
		t.Fatalf("listen at host error %v", err)
	}

	time.Sleep(time.Millisecond * 100)
	conn1, err := net.Dial("tcp", "127.0.0.1:2333")
	if err != nil {
		t.Fatalf("connect at host error %v", err)
	}

	conn2, err := net.Dial("tcp", "127.0.0.1:2333")
	if err != nil {
		t.Fatalf("connect at host error %v", err)
	}

	time.Sleep(time.Millisecond * 100)

	if len(testLoop.sEH.connMap) != 2 {
		t.Fatal("accepted connector error !")
	}

	conn1.Close()
	conn2.Close()

	if err = testLoop.Stop(); err != nil {
		t.Fatal("stop gnet loop")
	}
}
