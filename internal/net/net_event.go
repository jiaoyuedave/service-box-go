package net

//type BoxConn gnet.Conn

const (
	EventAccept      = iota //accept net connected from peer
	EventReqConnect         //request for connecting to remote
	EventRespConnect        //connect to remote successfully
	EventReceive            //receive Data
	EventClose              //Conn has been closed
	EventShutdown           //server shut down !
	EventMaxFlag            //network event  end
)

type (
	EventType uint8

	EventData struct {
		NetType EventType   //event type
		Data    interface{} //event Data
		Err     error       //err info
	}

	//AcceptEvent accept or connect
	AcceptEvent struct {
		Addr string
		Conn IBoxConn // net work channel
	}

	ConnectToEvent struct {
		Name    string
		Network string
		Addr    string
	}

	// ConnectEvent connect successfully
	ConnectEvent struct {
		Host string //peer address
		Conn IBoxConn
	}

	// ReceiveEvent Conn receive Data from peer
	ReceiveEvent struct {
		TSid string //peer address
		Data []byte //receive Data package
	}

	// ClosedEvent conn has been closed by network framework
	ClosedEvent struct {
		TSid string //Conn handle address
	}
)

// GetEventData TODO replace with object pool
func GetEventData() *EventData {
	return &EventData{}
}
