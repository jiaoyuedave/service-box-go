package zookeeper

type (
	zkEventType uint32
	zkEventData struct {
		eType zkEventType
		name  string
		path  string
		hosts []string
		err   error
	}
)

const (
	zkFindService zkEventType = iota + 1
	zkRegisterService
	zkUnRegisterService
	zkChangeHost
	zkDeleteHost
)
