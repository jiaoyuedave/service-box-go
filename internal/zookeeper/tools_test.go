package zookeeper

import "testing"

func TestGetNameFromPath(t *testing.T) {
	name := getNameFromPath("pre/1234567")
	if name != "1234567" {
		t.Fatalf("error")
	}
}
