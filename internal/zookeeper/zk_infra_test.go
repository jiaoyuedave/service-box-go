package zookeeper

import (
	"gitee.com/dennis-kk/service-box-go/util/service_infra"
	"testing"
)

type (
	testResult struct {
		hosts []string
		err   error
	}
)

var (
	res      *testResult
	zkClient *zkServiceInfra
)

func testOnServiceChange(name string, eventType service_infra.ChangedType, changeInfo *service_infra.ChangedInfo, err error) {
	switch eventType {
	case service_infra.ServiceChange:
		res.err = err
		res.hosts = changeInfo.Hosts
	case service_infra.ServiceDelete:
	}
}

func testInitZk(opts ...service_infra.Option) error {
	zkClient = makeZKServiceInfra().(*zkServiceInfra)

	if err := zkClient.Init(opts...); err != nil {
		return err
	}

	if err := zkClient.AddListener(testOnServiceChange); err != nil {
		return err
	}

	return zkClient.Start()
}

func TestStartZK(t *testing.T) {
	if err := testInitZk(service_infra.WithAppendHost("192.168.82.22")); err != nil {
		t.Fatalf("init zk module error %v", err)
	}

	if err := zkClient.Stop(); err != nil {
		t.Fatalf("stop service moudle error %v", err)
		return
	}
}

func TestZkServiceInfra_InitWithPrefix(t *testing.T) {
	if err := testInitZk(service_infra.WithAppendHost("192.168.82.22"), service_infra.WithPrefix("testcase")); err != nil {
		t.Fatalf("init zk module error %v", err)
	}

	exists, _, err := zkClient.conn.Exists("/testcase")
	if err != nil || exists != true {
		t.Fatalf("create prefix path error  %v!", err)
		return
	}

	if err := zkClient.Stop(); err != nil {
		t.Fatalf("stop service moudle error %v", err)
		return
	}
}

func TestZkServiceInfra_RegisterServiceWithPrefix(t *testing.T) {
	if err := testInitZk(service_infra.WithAppendHost("192.168.82.22"), service_infra.WithPrefix("testcase")); err != nil {
		t.Fatalf("init zk module error %v", err)
	}

	res = &testResult{}

	err := zkClient.RegisterService("testcaseService", &service_infra.ServiceInfo{Hosts: []string{"127.0.0.1:7999"}})
	if err != nil {
		t.Fatalf("register service with prefix error %v!", err)
		return
	}

	child, _, err := zkClient.conn.Children("/testcase/testcaseService")
	if err != nil {
		t.Fatalf("register service error %v", err)
		return
	}

	if child[0] != "127.0.0.1:7999" {
		t.Fatalf("unexcepted children node %v", child)
	}

	if err := zkClient.Stop(); err != nil {
		t.Fatalf("stop service moudle error %v", err)
		return
	}
}

func TestZkServiceInfra_RegisterService(t *testing.T) {
	if err := testInitZk(service_infra.WithAppendHost("192.168.82.22"), service_infra.WithPrefix("/")); err != nil {
		t.Fatalf("init zk module error %v", err)
	}

	res = &testResult{}

	err := zkClient.RegisterService("testcaseService", &service_infra.ServiceInfo{Hosts: []string{"127.0.0.1:7999"}})
	if err != nil {
		t.Fatalf("register service with prefix error %v!", err)
		return
	}

	child, _, err := zkClient.conn.Children("/testcaseService")
	if err != nil {
		t.Fatalf("register service error %v", err)
		return
	}

	if child[0] != "127.0.0.1:7999" {
		t.Fatalf("unexcepted children node %v", child)
	}

	if err := zkClient.Stop(); err != nil {
		t.Fatalf("stop service moudle error %v", err)
		return
	}
}
