package sbox

import (
	"gitee.com/dennis-kk/service-box-go/util/config"
	"testing"
)

const (
	testHostOne = `
listener:
-   host : "0.0.0.0:7999" #监听端口
    expose_host: "127.0.0.1:7999" #注册端口
`
	testNoexpose = `
listener:
-   host: "0.0.0.0:*"
`
)

var (
	testOpt Options
)

func init() {
	testOpt = Options{}
}

func TestParseHost(t *testing.T) {
	cfg, err := config.LoadYaml(testHostOne)
	if err != nil {
		t.Fatal("load yaml string from memory error ", err)
	}
	err = cfg.Get("listener").Scan(&testOpt.host)
	if err != nil {
		t.Fatal("scan listener config error ", err)
	}

	if len(testOpt.host) != 1 {
		t.Fatal("scan listener config error ")
	}

	if testOpt.host[0].InnerHost != "0.0.0.0:7999" {
		t.Fatal("inner host error exception 0.0.0.0:7999 recv ", testOpt.host[0].InnerHost)
	}

	if testOpt.host[0].PublicHost != "127.0.0.1:7999" {
		t.Fatal("inner host error exception 127.0.0.1:7999 recv ", testOpt.host[0].PublicHost)
	}
}

func TestGetHostIp(t *testing.T) {
	cfg, err := config.LoadYaml(testNoexpose)
	if err != nil {
		t.Fatal("load yaml string from memory error ", err)
	}
	err = cfg.Get("listener").Scan(&testOpt.host)
	if err != nil {
		t.Fatal("scan listener config error ", err)
	}

	for i := 0; i < 10; i++ {
		host := testOpt.GetOneHostIp()
		if host != "0.0.0.0:*" {
			t.Fatal("no exception ip ", host)
		}
	}
}
