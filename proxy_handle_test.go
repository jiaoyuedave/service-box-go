package sbox

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"testing"
	"time"

	"gitee.com/dennis-kk/rpc-go-backend/idlrpc"

	"gitee.com/dennis-kk/service-box-go/example/testcaller"

	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/protocol"
	"gitee.com/dennis-kk/service-box-go/example/idldata/pbdata"
	_ "gitee.com/dennis-kk/service-box-go/internal/jsonpb/jsonpbcpp"
	"google.golang.org/protobuf/proto"
)

type (
	testProxyClient struct {
		conn net.Conn
	}

	testProxyCaller struct {
		ch          chan bool
		box         *ServiceBox
		exceptProxy *testcaller.TestCallerProxy
		proxyCache  map[protocol.GlobalIndexType]idlrpc.IProxy
	}

	testcaseSDK struct {
		nickName   string
		testCaller testcaller.ITestCaller
	}
)

func (t *testcaseSDK) GetUuid() uint64 {
	return testcaller.SrvUUID
}

func (t *testcaseSDK) GetNickName() string {
	return t.nickName
}

func (t *testcaseSDK) IsProxy() bool {
	return false
}

func (t *testcaseSDK) Register(rpc idlrpc.IRpc) error {
	if rpc == nil {
		return errors.New("No effective rpc framework! ")
	}
	//register proxy
	err := rpc.AddProxyCreator(testcaller.SrvUUID, testcaller.TestCallerProxyCreator)
	if err != nil {
		return err
	}
	//register stub
	err = rpc.AddStubCreator(testcaller.SrvUUID, testcaller.TestCallerStubCreator)
	if err != nil {
		return err
	}
	//register service
	err = rpc.RegisterService(t.testCaller)
	if err != nil {
		return err
	}

	return nil
}

func initTestSDK(ops ...string) (idlrpc.ISDK, error) {
	sdk := &testcaseSDK{}
	sdk.testCaller = tCaller
	sdk.nickName = sdk.testCaller.GetNickName()
	if len(ops) > 1 {
		sdk.nickName = ops[0]
	}
	return sdk, nil
}

func (sp *testProxyCaller) GetNickName() string {
	return "6784101236246863201"
}

var (
	OutCallRetError      = errors.New("outside call inside return error")
	InsideCallTransError = errors.New("inside call outside error ")
	tCaller              = &testProxyCaller{
		proxyCache: make(map[protocol.GlobalIndexType]idlrpc.IProxy),
	}
)

func (sp *testProxyCaller) GetUUID() uint64 {
	return 6784101236246863201
}

func (sp *testProxyCaller) OnAfterFork(ctx context.Context) bool {
	sp.ch = make(chan bool, 1)
	sp.box = ctx.Value(SBoxKey{}).(*ServiceBox)
	return true
}

func (sp *testProxyCaller) OnTick() bool {
	//tick function in main goroutine
	return true
}

func (sp *testProxyCaller) OnBeforeDestroy() bool {
	//tick function in main goroutine
	return true
}
func (sp *testProxyCaller) SetInfo(ctx context.Context, _1 string) (err error) {
	proxy, err := sp.box.GetProxyFromPeer(ctx, 6784101236246863201)
	if err != nil {
		return
	}

	callerProxy := proxy.(*testcaller.TestCallerProxy)
	msg, err := callerProxy.GetInfo()
	if err != nil {
		sp.ch <- false
		return nil
	}

	if msg == "hello plato" {
		sp.ch <- true
	} else {
		sp.ch <- false
	}

	return
}

func (sp *testProxyCaller) GetInfo(ctx context.Context) (ret1 string, err error) {
	proxy, err := sp.box.GetProxyFromPeer(ctx, 6784101236246863201)
	if err != nil {
		return
	}

	sp.exceptProxy = proxy.(*testcaller.TestCallerProxy)
	sp.proxyCache[sp.exceptProxy.GetGlobalIndex()] = sp.exceptProxy
	return
}

func (pc *testProxyClient) init(address string) error {
	var err error
	pc.conn, err = net.Dial("tcp", address)
	return err
}

func (pc *testProxyClient) sendRpcMethod() {
	pbarg := &pbdata.TestCallee_AddArgs{
		Arg1: 1,
		Arg2: 2,
	}
	pkg, _ := proto.Marshal(pbarg)

	reqPb := &protocol.RequestPackage{
		Header: &protocol.RpcCallHeader{
			RpcMsgHeader: protocol.RpcMsgHeader{
				Length: uint32(protocol.CallHeadSize + len(pkg)),
				Type:   protocol.RequestMsg,
			},
			ServiceUUID: callee.ServiceUUID,
			ServerID:    1,
			CallID:      1,
			MethodID:    1,
		},
		Buffer: pkg,
	}

	buffer, _ := protocol.PackReqMsg(reqPb)

	_, _ = pc.conn.Write(buffer)
}

func (pc *testProxyClient) checkCalleeReturn() error {
	buff := make([]byte, 256)
	_, err := pc.conn.Read(buff)
	if err != nil {
		return err
	}

	header := protocol.ReadRetHeader(buff[:protocol.RespHeadSize])
	if header.Type != protocol.ResponseMsg {
		return OutCallRetError
	}

	if header.ErrorCode != protocol.IDL_SUCCESS {
		return OutCallRetError
	}

	// 比较大小
	pbret := &pbdata.TestCallee_AddRet{
		Ret1: 3,
	}

	pkg, _ := proto.Marshal(pbret)

	if header.Length != uint32(protocol.RespHeadSize+len(pkg)) {
		return OutCallRetError
	}
	// 尝试解析
	revResp := &pbdata.TestCallee_AddRet{}
	err = proto.Unmarshal(buff[protocol.RespHeadSize:header.Length], revResp)
	if err != nil {
		return err
	}

	if revResp.Ret1 != pbret.Ret1 {
		return OutCallRetError
	}

	return nil
}

func (pc *testProxyClient) sendCallerMethod() {
	pbarg := &pbdata.TestCaller_SetInfoArgs{
		Arg1: "hello plato",
	}
	pkg, _ := proto.Marshal(pbarg)

	reqPb := &protocol.RequestPackage{
		Header: &protocol.RpcCallHeader{
			RpcMsgHeader: protocol.RpcMsgHeader{
				Length: uint32(protocol.CallHeadSize + len(pkg)),
				Type:   protocol.RequestMsg,
			},
			ServiceUUID: caller.ServiceUUID,
			ServerID:    1,
			CallID:      2,
			MethodID:    1,
		},
		Buffer: pkg,
	}

	buffer, _ := protocol.PackReqMsg(reqPb)

	_, _ = pc.conn.Write(buffer)
}

func (pc *testProxyClient) sendCallerGetInfo() {
	pbarg := &pbdata.TestCaller_GetInfoArgs{}
	pkg, _ := proto.Marshal(pbarg)

	reqPb := &protocol.RequestPackage{
		Header: &protocol.RpcCallHeader{
			RpcMsgHeader: protocol.RpcMsgHeader{
				Length: uint32(protocol.CallHeadSize + len(pkg)),
				Type:   protocol.RequestMsg,
			},
			ServiceUUID: caller.ServiceUUID,
			ServerID:    1,
			CallID:      3,
			MethodID:    2,
		},
		Buffer: pkg,
	}

	buffer, _ := protocol.PackReqMsg(reqPb)

	_, _ = pc.conn.Write(buffer)
}

func (pc *testProxyClient) checkCallerReturn() error {
	buff := make([]byte, 256)
	_, err := pc.conn.Read(buff)
	if err != nil {
		return err
	}

	header := protocol.ReadRetHeader(buff[:protocol.RespHeadSize])
	if header.Type != protocol.ResponseMsg {
		return fmt.Errorf("not correct resp msg type %d", header.Type)
	}

	if header.ErrorCode != protocol.IDL_SUCCESS {
		return errors.New("call remote method not successuf")
	}

	if header.Length-uint32(protocol.RespHeadSize) != 0 {
		return errors.New("not corrrect body lenght")
	}

	if header.CallID != 2 {
		return fmt.Errorf("not correct call id %d", header.CallID)
	}

	return nil
}

func (pc *testProxyClient) checkNotFoundRet() error {
	buff := make([]byte, 256)
	_, err := pc.conn.Read(buff)
	if err != nil {
		return err
	}

	header := protocol.ReadRetHeader(buff[:protocol.RespHeadSize])
	if header.Type != protocol.ResponseMsg {
		return OutCallRetError
	}

	if header.ErrorCode != protocol.IDL_SERVICE_NOT_FOUND {
		return OutCallRetError
	}
	return nil
}

func (pc *testProxyClient) checkInsideCaller() error {
	buff := make([]byte, 256)
	_, err := pc.conn.Read(buff)
	if err != nil {
		return err
	}

	header := protocol.ReadCallHeader(buff[:protocol.CallHeadSize])
	if header.Type != protocol.RequestMsg {
		return OutCallRetError
	}

	if header.MethodID != 2 {
		return InsideCallTransError
	}

	//return code to inside server

	pbarg := &pbdata.TestCaller_GetInfoRet{
		Ret1: "hello plato",
	}
	pkg, _ := proto.Marshal(pbarg)

	reqPb := &protocol.ResponsePackage{
		Header: &protocol.RpcCallRetHeader{
			RpcMsgHeader: protocol.RpcMsgHeader{
				Length: uint32(protocol.RespHeadSize + len(pkg)),
				Type:   protocol.ResponseMsg,
			},
			ServerID: header.ServerID,
			CallID:   header.CallID,
		},
		Buffer: pkg,
	}

	msg, _ := protocol.PackRespMsg(reqPb)
	pc.conn.Write(msg)
	return nil
}

func (pc *testProxyClient) Stop() {
	pc.conn.Close()
}

func TestProxyStart(t *testing.T) {
	appProxy := testApp{}
	if err := appProxy.Init([]string{"proxy_app", "-c", "default.yaml", "-p", "true"}); err != nil {
		t.Fatal(err)
	}
	if err := appProxy.Start(); err != nil {
		t.Fatalf("start box error %v", err)
	}
	time.Sleep(100 * time.Millisecond)
	appProxy.Stop()
}

func TestOutCallInside(t *testing.T) {
	appProxy := testApp{}
	appSrv := testApp{}
	client := testProxyClient{}

	if err := appProxy.Init([]string{"proxy_app", "-c", "default.yaml", "-p", "true"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.Init([]string{"inside_server", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.box.AddServicePackage(callee); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := appProxy.Start(); err != nil {
		t.Fatalf("start proxy box error %v", err)
	}

	if err := appSrv.Start(); err != nil {
		t.Fatalf("start service box error %v", err)
	}

	time.Sleep(time.Millisecond * 100)

	if err := client.init(appProxy.box.proxy.cfg.Address); err != nil {
		t.Fatalf("connect to proxy error %v", err)
	}
	time.Sleep(time.Millisecond * 100)
	client.sendRpcMethod()

	time.Sleep(time.Millisecond * 100)

	if err := client.checkCalleeReturn(); err != nil {
		t.Fatalf("connect to proxy return %v", err)
	}

	client.sendRpcMethod()

	time.Sleep(time.Millisecond * 100)

	if err := client.checkCalleeReturn(); err != nil {
		t.Fatalf("connect to proxy return %v", err)
	}

	client.Stop()

	appProxy.Stop()
	appSrv.Stop()
}

func TestInvalidMethod(t *testing.T) {
	appProxy := testApp{}

	client := testProxyClient{}

	if err := appProxy.Init([]string{"proxy_app", "-c", "default.yaml", "-p", "true"}); err != nil {
		t.Fatal(err)
	}

	if err := appProxy.Start(); err != nil {
		t.Fatalf("start proxy box error %v", err)
	}

	time.Sleep(time.Millisecond * 100)

	if err := client.init(appProxy.box.proxy.cfg.Address); err != nil {
		t.Fatalf("connect to proxy error %v", err)
	}

	client.sendCallerMethod()

	time.Sleep(time.Millisecond * 100)

	if err := client.checkNotFoundRet(); err != nil {
		t.Fatalf("connect to proxy return %v", err)
	}

	appProxy.Stop()
}

func TestInsideCall(t *testing.T) {
	appProxy := testApp{}
	appSrv := testApp{}
	client := testProxyClient{}

	if err := appProxy.Init([]string{"proxy_app", "-c", "default.yaml", "-p", "true"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.Init([]string{"inside_server", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}
	caller.Creator = initTestSDK
	if err := appSrv.box.AddServicePackage(caller); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := appProxy.Start(); err != nil {
		t.Fatalf("start proxy box error %v", err)
	}

	if err := appSrv.Start(); err != nil {
		t.Fatalf("start service box error %v", err)
	}

	time.Sleep(time.Millisecond * 100)
	if err := client.init(appProxy.box.proxy.cfg.Address); err != nil {
		t.Fatalf("connect to proxy error %v", err)
	}

	client.sendCallerMethod()

	// wait for caller
	if err := client.checkInsideCaller(); err != nil {
		t.Fatalf("connect to proxy return %v", err)
	}

	res := <-tCaller.ch
	if res == false {
		t.Fatalf("connect to proxy return")
	}

	client.Stop()

	appProxy.Stop()
	appSrv.Stop()
}

func TestInsideCallCloseOutSide(t *testing.T) {

	appProxy := testApp{}
	appSrv := testApp{}
	client := testProxyClient{}

	if err := appProxy.Init([]string{"proxy_app", "-c", "default.yaml", "-p", "true"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.Init([]string{"inside_server", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}

	caller.Creator = initTestSDK
	if err := appSrv.box.AddServicePackage(caller); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := appProxy.Start(); err != nil {
		t.Fatalf("start proxy box error %v", err)
	}

	if err := appSrv.Start(); err != nil {
		t.Fatalf("start service box error %v", err)
	}

	time.Sleep(time.Millisecond * 50)
	if err := client.init(appProxy.box.proxy.cfg.Address); err != nil {
		t.Fatalf("connect to proxy error %v", err)
	}

	time.Sleep(time.Millisecond * 50)

	client.sendCallerMethod()

	client.Stop()

	res := <-tCaller.ch
	if res != false {
		t.Fatalf("not recive correct error !")
	}

	appProxy.Stop()
	appSrv.Stop()

}

func TestProxyGlobalIndex(t *testing.T) {
	appProxy := testApp{}
	appSrv := testApp{}
	client := testProxyClient{}

	if err := appProxy.Init([]string{"proxy_app", "-c", "default.yaml", "-p", "true"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.Init([]string{"inside_server", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}

	caller.Creator = initTestSDK
	if err := appSrv.box.AddServicePackage(caller); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := appProxy.Start(); err != nil {
		t.Fatalf("start proxy box error %v", err)
	}

	if err := appSrv.Start(); err != nil {
		t.Fatalf("start service box error %v", err)
	}

	time.Sleep(time.Millisecond * 50)
	if err := client.init(appProxy.box.proxy.cfg.Address); err != nil {
		t.Fatalf("connect to proxy error %v", err)
	}

	client.sendCallerGetInfo()

	time.Sleep(time.Millisecond * 50)

	if tCaller.exceptProxy.GetGlobalIndex() == 0 {
		t.Fatalf("error global index %d", tCaller.exceptProxy.GetGlobalIndex())
	}

	client.Stop()
	appProxy.Stop()
	appSrv.Stop()
}

func TestOutCloseBeforeReturn(t *testing.T) {
	appProxy := testApp{}
	appSrv := testApp{}
	client := testProxyClient{}

	if err := appProxy.Init([]string{"proxy_app", "-c", "default.yaml", "-p", "true"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.Init([]string{"inside_server", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.box.AddServicePackage(callee); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := appProxy.Start(); err != nil {
		t.Fatalf("start proxy box error %v", err)
	}

	if err := appSrv.Start(); err != nil {
		t.Fatalf("start service box error %v", err)
	}

	time.Sleep(time.Millisecond * 100)

	if err := client.init(appProxy.box.proxy.cfg.Address); err != nil {
		t.Fatalf("connect to proxy error %v", err)
	}
	time.Sleep(time.Millisecond * 100)
	for id := 0; id < 10; id++ {
		client.sendRpcMethod()
	}
	err := client.conn.Close()
	if err != nil {
		return
	}

	time.Sleep(time.Millisecond * 100)
	appProxy.Stop()
	appSrv.Stop()
}

//多个客户端获取通过网关连接进来，反向获取proxy
func TestMultiOutSideProxy(t *testing.T) {
	appProxy := testApp{}
	appSrv := testApp{}
	client1 := testProxyClient{}
	client2 := testProxyClient{}

	if err := appProxy.Init([]string{"proxy_app", "-c", "default.yaml", "-p", "true"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.Init([]string{"inside_server", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}

	caller.Creator = initTestSDK
	if err := appSrv.box.AddServicePackage(caller); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := appProxy.Start(); err != nil {
		t.Fatalf("start proxy box error %v", err)
	}

	if err := appSrv.Start(); err != nil {
		t.Fatalf("start service box error %v", err)
	}

	time.Sleep(time.Millisecond * 50)

	if err := client1.init(appProxy.box.proxy.cfg.Address); err != nil {
		t.Fatalf("connect to proxy error %v", err)
	}

	if err := client2.init(appProxy.box.proxy.cfg.Address); err != nil {
		t.Fatalf("connect to proxy error %v", err)
	}

	time.Sleep(time.Millisecond * 50)

	client1.sendCallerGetInfo()
	client2.sendCallerGetInfo()

	time.Sleep(time.Millisecond * 50)

	if len(tCaller.proxyCache) != 2 {
		t.Fatalf("test mutli client test error %d！", len(tCaller.proxyCache))
	}

	client1.Stop()
	client2.Stop()

	appSrv.Stop()
	appProxy.Stop()
}

func TestInsideServiceRestart(t *testing.T) {
	appProxy := testApp{}
	appSrv := testApp{}
	client := testProxyClient{}

	if err := appProxy.Init([]string{"proxy_app", "-c", "default.yaml", "-p", "true"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.Init([]string{"inside_server", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}

	caller.Creator = initTestSDK
	if err := appSrv.box.AddServicePackage(caller); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := appProxy.Start(); err != nil {
		t.Fatalf("start proxy box error %v", err)
	}

	if err := appSrv.Start(); err != nil {
		t.Fatalf("start service box error %v", err)
	}

	time.Sleep(time.Millisecond * 50)

	//关闭然后重启
	appSrv.Stop()
	appSrv = testApp{}
	if err := appSrv.Init([]string{"inside_server", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}

	caller.Creator = initTestSDK
	if err := appSrv.box.AddServicePackage(caller); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := appSrv.Start(); err != nil {
		t.Fatalf("start service box error %v", err)
	}
	time.Sleep(time.Millisecond * 50)

	if err := client.init(appProxy.box.proxy.cfg.Address); err != nil {
		t.Fatalf("connect to proxy error %v", err)
	}

	client.sendCallerMethod()

	// time.Sleep(time.Millisecond * 50)

	// client.sendCallerMethod()
	// wait for caller
	if err := client.checkInsideCaller(); err != nil {
		t.Fatalf("connect to proxy return %v", err)
	}

	res := <-tCaller.ch
	if res == false {
		t.Fatalf("connect to proxy return")
	}

	client.Stop()
	appSrv.Stop()
	appProxy.Stop()
}

func TestServiceClose(t *testing.T) {
	appProxy := testApp{}
	appSrv := testApp{}
	client := testProxyClient{}

	if err := appProxy.Init([]string{"proxy_app", "-c", "default.yaml", "-p", "true"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.Init([]string{"inside_server", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.box.AddServicePackage(callee); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := appProxy.Start(); err != nil {
		t.Fatalf("start proxy box error %v", err)
	}

	if err := appSrv.Start(); err != nil {
		t.Fatalf("start service box error %v", err)
	}

	if err := client.init(appProxy.box.proxy.cfg.Address); err != nil {
		t.Fatalf("connect to proxy error %v", err)
	}

	client.sendRpcMethod()

	if err := client.checkCalleeReturn(); err != nil {
		t.Fatalf("connect to proxy return %v", err)
	}
	// 停机
	appSrv.Stop()
	time.Sleep(time.Millisecond * 100)
	client.sendRpcMethod()
	if err := client.checkNotFoundRet(); err != nil {
		t.Fatalf("connect to proxy return %v", err)
	}
	client.Stop()
	appProxy.Stop()
}

//TestMultiServiceInSameApplication 测试通过网关发现同一个进程内的两个服务
func TestMultiServiceInSameApplication(t *testing.T) {
	appProxy := testApp{}
	appSrv := testApp{}
	client := testProxyClient{}

	caller = &idlrpc.PackageInfo{
		ServiceUUID: testcaller.SrvUUID,
		Creator:     testcaller.InitSDK,
	}

	if err := appProxy.Init([]string{"proxy_app", "-c", "default.yaml", "-p", "true"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.Init([]string{"inside_server", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.box.AddServicePackage(callee); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := appSrv.box.AddServicePackage(caller); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := appProxy.Start(); err != nil {
		t.Fatalf("start proxy box error %v", err)
	}

	if err := appSrv.Start(); err != nil {
		t.Fatalf("start service box error %v", err)
	}

	if err := client.init(appProxy.box.proxy.cfg.Address); err != nil {
		t.Fatalf("connect to proxy error %v", err)
	}

	//调用callee 的方法
	client.sendRpcMethod()
	//间隔发送触发分别发现的逻辑
	if err := client.checkCalleeReturn(); err != nil {
		t.Fatalf("connect to proxy return %v", err)
	}
	//调用caller 的方法
	client.sendCallerMethod()

	if err := client.checkCallerReturn(); err != nil {
		t.Fatalf("connect to proxy return %v", err)
	}

	client.Stop()
	appSrv.Stop()
	appProxy.Stop()
}

func TestProxyRestart(t *testing.T) {
	//初始化客户端环境
	appProxy := testApp{}
	appSrv := testApp{}

	client := testProxyClient{}

	if err := appProxy.Init([]string{"proxy_app", "-c", "default.yaml", "-p", "true"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.Init([]string{"inside_server", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}

	caller.Creator = initTestSDK
	if err := appSrv.box.AddServicePackage(caller); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := appProxy.Start(); err != nil {
		t.Fatalf("start proxy box error %v", err)
	}

	if err := appSrv.Start(); err != nil {
		t.Fatalf("start service box error %v", err)
	}

	time.Sleep(time.Millisecond * 50)
	if err := client.init(appProxy.box.proxy.cfg.Address); err != nil {
		t.Fatalf("connect to proxy error %v", err)
	}
	// 调用一次请求
	client.sendCallerGetInfo()
	time.Sleep(time.Millisecond * 50)
	// 重启网关
	appProxy.Stop()

	//重新产生一个新网关
	appProxy = testApp{}
	if err := appProxy.Init([]string{"proxy_app", "-c", "default.yaml", "-p", "true"}); err != nil {
		t.Fatal(err)
	}

	if err := appProxy.Start(); err != nil {
		t.Fatalf("start proxy box error %v", err)
	}

	time.Sleep(time.Millisecond * 50)

	if err := client.init(appProxy.box.proxy.cfg.Address); err != nil {
		t.Fatalf("connect to proxy error %v", err)
	}

	client.sendCallerMethod()

	if err := client.checkInsideCaller(); err != nil {
		t.Fatalf("connect to proxy return %v", err)
	}

	res := <-tCaller.ch
	if res == false {
		t.Fatalf("connect to proxy return")
	}

	client.Stop()
	appSrv.Stop()
	appProxy.Stop()

}

//测试http 模块初始化
func TestHttpProxyStart(t *testing.T) {
	//初始化客户端环境
	appProxy := testApp{}
	if err := appProxy.Init([]string{"proxy_app", "-c", "test_http_proxy.yaml", "-p", "true"}); err != nil {
		t.Fatal(err)
	}

	if err := appProxy.Start(); err != nil {
		t.Fatalf("start proxy box error %v", err)
	}

	time.Sleep(time.Millisecond * 50)
	appProxy.Stop()
}

func TestHttpProxy_CallMethod(t *testing.T) {
	appProxy := testApp{}
	appSrv := testApp{}

	if err := appProxy.Init([]string{"proxy_app", "-c", "test_http_proxy.yaml", "-p", "true"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.Init([]string{"inside_server", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.box.AddServicePackage(callee); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := appProxy.Start(); err != nil {
		t.Fatalf("start proxy box error %v", err)
	}

	if err := appSrv.Start(); err != nil {
		t.Fatalf("start service box error %v", err)
	}

	client := &http.Client{}
	jsonBody := []byte(`{"args": {"arg1": 233, "arg2": 466}}`)
	bodyReader := bytes.NewReader(jsonBody)
	req, err := http.NewRequest("POST", "http://0.0.0.0:6060/TestCallee/Add", bodyReader)
	if err != nil {
		t.Fatal(err)
	}
	resp, err := client.Do(req)
	if resp == nil {
		t.Fatalf("no effective response %s", err)
	}

	//测试状态
	if resp.StatusCode != http.StatusOK {
		body, _ := ioutil.ReadAll(resp.Body)
		t.Errorf("unexception error code %d error %s", resp.StatusCode, body)
	}

	// 测试回包
	body, _ := ioutil.ReadAll(resp.Body)
	if string(body[:]) != "\"{\\n\\t\\\"ret1\\\" : 699\\n}\\n\"" {
		t.Fatalf("unexcepted http response %s ! ", string(body[:]))
	}

	appSrv.Stop()
	appProxy.Stop()
}

func TestHttpProxy_CallServiceNotFound(t *testing.T) {
	appProxy := testApp{}
	appSrv := testApp{}

	if err := appProxy.Init([]string{"proxy_app", "-c", "test_http_proxy.yaml", "-p", "true"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.Init([]string{"inside_server", "-c", "test_app.yaml"}); err != nil {
		t.Fatal(err)
	}

	if err := appSrv.box.AddServicePackage(callee); err != nil {
		t.Fatalf("Add callee error %v", err)
	}

	if err := appProxy.Start(); err != nil {
		t.Fatalf("start proxy box error %v", err)
	}

	if err := appSrv.Start(); err != nil {
		t.Fatalf("start service box error %v", err)
	}

	client := &http.Client{}
	jsonBody := []byte(`{"args": {"arg1": 233}}`)
	bodyReader := bytes.NewReader(jsonBody)
	req, err := http.NewRequest("POST", "http://0.0.0.0:6060/TestCaller/SetInfo", bodyReader)
	if err != nil {
		t.Fatal(err)
	}
	resp, err := client.Do(req)
	if resp == nil {
		t.Fatalf("no effective response %s", err)
	}

	//测试状态
	if resp.StatusCode != http.StatusGatewayTimeout {
		body, _ := ioutil.ReadAll(resp.Body)
		t.Errorf("unexception error code %d error %s", resp.StatusCode, body)
	}

	appSrv.Stop()
	appProxy.Stop()
}
