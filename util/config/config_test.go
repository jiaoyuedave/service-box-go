package config

import (
	"os"
	"testing"

	"gitee.com/dennis-kk/service-box-go/util/config/source/apollo"
)

func init() {
	err := os.Setenv("BOX_NAME", "test-apollo-app")
	if err != nil {
		panic(err)
	}
}

func TestLoadApollo(t *testing.T) {
	aCfg := &apollo.Config{
		Host:      "http://106.54.227.205:8080",
		AppId:     "cloudguan-app",
		Cluster:   "default",
		NameSpace: "plato-service.yaml",
		Secret:    "ac9179dd8d044605bcd77f0f94ed86f0",
	}

	cfg, err := LoadApollo(aCfg)
	if err != nil {
		t.Fatalf("load apollo source failed %v !", err)
	}

	if cfg == nil {
		t.Fatalf("invalid apollo cfg")
	}

	if cfg.Get("name").String("") != "test-apollo-app" {
		t.Errorf("unexcept app name %s", cfg.Get("name").String(""))
	}

	serviceFinder := cfg.Get("service_finder").StringMap(nil)
	if serviceFinder == nil {
		t.Errorf("unexcept service_finder %v", serviceFinder)
	}
}

func TestMultiNameSpaceConfig(t *testing.T) {
	aCfg := &apollo.Config{
		Host:      "http://106.54.227.205:8080",
		AppId:     "cloudguan-app",
		Cluster:   "default",
		NameSpace: "plato-service.yaml,plato-mutil.yaml",
		Secret:    "ac9179dd8d044605bcd77f0f94ed86f0",
	}

	cfg, err := LoadApollo(aCfg)
	if err != nil {
		t.Fatalf("load apollo source failed %v !", err)
	}

	if cfg == nil {
		t.Fatalf("invalid apollo cfg")
	}

	if cfg.Get("name").String("") != "plato_mutil" {
		t.Errorf("unexcept app name %s", cfg.Get("name").String(""))
	}

	testName := cfg.Get("mutil_test", "name").String("")
	if testName != "plato config" {
		t.Errorf("unexcept mutil_test.name %q", testName)
	}

	testValue := cfg.Get("mutil_test", "vlaue").Int(0)
	if testValue != 12345 {
		t.Errorf("unexcept mutil_test.vlaue %d", testValue)
	}
}
