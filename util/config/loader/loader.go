package loader

import (
	"errors"
	"gitee.com/dennis-kk/service-box-go/util/config/source"
)

var (
	ErrNotLoaded = errors.New("not loaded config yet")
)

// Loader manages loading sources
type Loader interface {
	// Close will stop the loader
	Close() error
	// Load the sources
	Load(...source.Source) error
	// A Snapshot of loaded config
	Snapshot() (*Snapshot, error)
	// String is the name of loader
	String() string
}

// Snapshot is a merged ChangeSet
type Snapshot struct {
	// The merged ChangeSet
	ChangeSet *source.ChangeSet
	// Deterministic and comparable version of the snapshot
	Version string
}

// Copy snapshot
func Copy(s *Snapshot) *Snapshot {
	cs := *(s.ChangeSet)

	return &Snapshot{
		ChangeSet: &cs,
		Version:   s.Version,
	}
}
