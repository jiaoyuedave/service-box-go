// Package memory loader will load config info from source, encoder it and store to memory
package memory

import (
	"errors"
	"fmt"
	"gitee.com/dennis-kk/service-box-go/util/config/loader"
	"gitee.com/dennis-kk/service-box-go/util/config/reader"
	"gitee.com/dennis-kk/service-box-go/util/config/source"
	"strings"
	"sync"
	"time"
)

type memory struct {
	opts loader.Options

	sync.RWMutex
	// the current snapshot
	snap *loader.Snapshot
	// the current values
	vals reader.Values
	// all the sets
	sets []*source.ChangeSet
	// all the sources
	sources []source.Source
}

func genVer() string {
	return fmt.Sprintf("%d", time.Now().UnixNano())
}

func (m *memory) loaded() bool {
	var loaded bool
	m.RLock()
	if m.vals != nil {
		loaded = true
	}
	m.RUnlock()
	return loaded
}

// Snapshot returns a snapshot of the current loaded config
func (m *memory) Snapshot() (*loader.Snapshot, error) {
	// make copy
	m.RLock()
	snap := loader.Copy(m.snap)
	m.RUnlock()
	return snap, nil
}

//Close will clear nothing for yaml type
func (m *memory) Close() error {
	return nil
}

func (m *memory) Get(path ...string) (reader.Value, error) {
	if !m.loaded() {
		return nil, loader.ErrNotLoaded
	}

	m.Lock()
	defer m.Unlock()

	// did sync actually work?
	if m.vals != nil {
		return m.vals.Get(path...), nil
	}

	// assuming vals is nil
	// create new vals

	ch := m.snap.ChangeSet

	// we are truly screwed, trying to load in a hacked way
	v, err := m.opts.Reader.Values(ch)
	if err != nil {
		return nil, err
	}

	// lets set it just because
	m.vals = v

	if m.vals != nil {
		return m.vals.Get(path...), nil
	}

	// ok we're going hardcore now
	return nil, errors.New("no values")
}

func (m *memory) Load(sources ...source.Source) error {
	var gerrors []string

	for _, memSource := range sources {
		set, err := memSource.Read()
		if err != nil {
			gerrors = append(gerrors,
				//FIXME change to box's log
				fmt.Sprintf("error loading memSouce %s: %v",
					memSource,
					err))
			continue
		}

		m.Lock()
		m.sources = append(m.sources, memSource)
		m.sets = append(m.sets, set)
		m.Unlock()
	}
	//TODO implement reload

	//try load config
	m.Lock()
	// merge sets
	set, err := m.opts.Reader.Merge(m.sets...)
	if err != nil {
		m.Unlock()
		return err
	}

	// set values
	vals, err := m.opts.Reader.Values(set)
	if err != nil {
		m.Unlock()
		return err
	}
	m.vals = vals
	m.snap = &loader.Snapshot{
		ChangeSet: set,
		Version:   genVer(),
	}
	m.Unlock()

	// Return errors
	if len(gerrors) != 0 {
		//TODO add box's log
		return errors.New(strings.Join(gerrors, "\n"))
	}
	return nil
}

func (m *memory) String() string {
	return "memory"
}

func NewLoader(opts ...loader.Option) loader.Loader {
	options := loader.Options{}

	for _, o := range opts {
		o(&options)
	}

	m := &memory{
		opts:    options,
		sources: options.Source,
	}

	return m
}
