package config

import (
	"gitee.com/dennis-kk/service-box-go/util/config/loader"
	"gitee.com/dennis-kk/service-box-go/util/config/reader"
	"gitee.com/dennis-kk/service-box-go/util/config/source"
)

type Options struct {
	Loader loader.Loader
	Reader reader.Reader
	//Source is cache of options
	Source []source.Source
}

//Option is the decorators func of change options variable
type Option func(o *Options)

// WithLoader sets the loader for manager config
func WithLoader(l loader.Loader) Option {
	return func(o *Options) {
		o.Loader = l
	}
}

// WithSource appends a source to list of sources
func WithSource(s source.Source) Option {
	return func(o *Options) {
		o.Source = append(o.Source, s)
	}
}

// WithReader sets the config reader
func WithReader(r reader.Reader) Option {
	return func(o *Options) {
		o.Reader = r
	}
}
