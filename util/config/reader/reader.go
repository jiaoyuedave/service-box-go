package reader

import (
	"gitee.com/dennis-kk/service-box-go/util/config/source"
	"time"
)

// Reader is an interface for merging changesets
type Reader interface {
	Merge(...*source.ChangeSet) (*source.ChangeSet, error)
	Values(*source.ChangeSet) (Values, error)
	String() string
}

// Values is returned by the reader
type Values interface {
	Bytes() []byte
	Get(path ...string) Value
	Map() map[string]interface{}
	Scan(v interface{}) error //will unmarshall to readable struct
}

// Value represents a value of any type
type Value interface {
	IsNil() bool
	Bool(def bool) bool
	Int(def int) int
	String(def string) string
	Float64(def float64) float64
	Duration(def time.Duration) time.Duration
	StringSlice(def []string) []string
	StringMap(def map[string]string) map[string]string
	Scan(val interface{}) error
	Bytes() []byte
	Get(key string) Value
}
