package apollo

import (
	"fmt"
	"gitee.com/dennis-kk/service-box-go/util/errors"
	"testing"
)

var (
	testNP = []byte(`name: testapp1

listener:
-   host : "0.0.0.0:7999" #监听端口
    expose_host: "127.0.0.1:7999" #注册端口

service_finder:
    type: zookeeper
    host: 127.0.0.1:2181 #zk 地址

rpc:
    stack_trace: true #panic时，是开启堆栈打印

uuid:
    custom:
        seed: 233

connect:
    host: 127.0.0.1:6999 # 直连进程地址

proxy:
    host: 127.0.0.1:6666 #proxy 模式监听地址


logger:
    level: debug
    mode: ["console"]
    outfile: service-box.log
    style: console
    caller_skip_count: 2

services:
    custom_name:
        5871407834537456905: "other_login"
        5871407834219999997: "other_service"
        8590810067174448468: "test_case_callee"`)
)

func TestApolloConfig(t *testing.T) {

	var apolloTest = []struct {
		input     string
		except    Config
		exceptErr error
	}{
		{"", Config{}, errors.InvalidApolloCfg},
		{"https://test.apollo.com", Config{}, errors.InvalidApolloCfg},
		{"https://test.apollo.com test-game-app", Config{Host: "https://test.apollo.com", AppId: "test-game-app"}, nil},
	}

	for _, tCfg := range apolloTest {
		cfg := Config{}
		err := cfg.Set(tCfg.input)
		if err != tCfg.exceptErr {
			t.Errorf(fmt.Sprintf("unexcpetion err recv %v except %v", err, tCfg.exceptErr))
		}
	}
}

func TestHttpReadYaml(t *testing.T) {
	ap := apolloCenter{
		cfg: &Config{
			Host:      "http://106.54.227.205:8080",
			AppId:     "cloudguan-app",
			Cluster:   "default",
			NameSpace: "plato-service.yaml",
			Secret:    "ac9179dd8d044605bcd77f0f94ed86f0",
			Auth:      &AuthSignature{},
			ConfigExt: configFormat{
				".yaml": true,
			},
		},
	}

	ch, err := ap.Read()
	if err != nil {
		return
	}

	if ch.Data == nil {
		t.Fatalf("unexcption config data ")
		return
	}
}

func TestHttpReadProperties(t *testing.T) {
	ap := apolloCenter{
		cfg: &Config{
			Host:      "http://106.54.227.205:8080",
			AppId:     "cloudguan-app",
			Cluster:   "default",
			NameSpace: "application",
			Secret:    "ac9179dd8d044605bcd77f0f94ed86f0",
			Auth:      &AuthSignature{},
			ConfigExt: configFormat{
				".yaml": true,
			},
		},
	}

	ch, err := ap.Read()
	if err != nil {
		return
	}

	if ch.Data == nil {
		t.Fatalf("unexcption config data ")
		return
	}
}
