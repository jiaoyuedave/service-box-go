package apollo

import "testing"

const (
	rawURL = "http://baidu.com/a/b?key=1"
	secret = "6ce3ff7e96a24335a9634fe9abca6d51"
	appID  = "testApplication_yang"
)

func TestSignString(t *testing.T) {
	s := signString(rawURL, secret)
	if s != "mcS95GXa7CpCjIfrbxgjKr0lRu8=" {
		t.Errorf("TestSignString Failed")
	}
}

func TestUrl2PathWithQuery(t *testing.T) {

	pathWithQuery := url2PathWithQuery(rawURL)

	if pathWithQuery != "/a/b?key=1" {
		t.Errorf("TestUrl2PathWithQuery Failed")
	}
}

//func TestHttpHeaders(t *testing.T) {
//	a := &AuthSignature{}
//	headers := a.HTTPHeaders(rawURL, appID, secret)
//
//}
