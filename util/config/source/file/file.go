package file

import (
	"gitee.com/dennis-kk/service-box-go/util/config/encoder"
	"gitee.com/dennis-kk/service-box-go/util/config/source"
	"io/ioutil"
	"os"
	"strings"
)

func format(p string, e encoder.Encoder) string {
	parts := strings.Split(p, ".")
	if len(parts) > 1 {
		return parts[len(parts)-1]
	}
	return e.String()
}

type file struct {
	path string
	opts *source.Options
}

func (f *file) Read() (*source.ChangeSet, error) {
	fh, err := os.Open(f.path)
	if err != nil {
		return nil, err
	}
	defer fh.Close()
	//read binary array form file
	b, err := ioutil.ReadAll(fh)
	if err != nil {
		return nil, err
	}
	//get file description
	info, err := fh.Stat()
	if err != nil {
		return nil, err
	}

	//处理环境变量，替换环境变量为对应的值
	b = []byte(os.ExpandEnv(string(b)))

	cs := &source.ChangeSet{
		Format:    format(f.path, f.opts.Encoder),
		Source:    f.String(),
		Timestamp: info.ModTime(),
		Data:      b,
	}
	cs.Checksum = cs.Sum()

	return cs, nil
}

func (f *file) String() string {
	return "file"
}

func NewSource(opts ...source.Option) source.Source {
	options := source.NewOptions(opts...)
	path := "default.yaml"
	f, ok := options.Context.Value(filePathKey{}).(string)
	if ok {
		path = f
	}
	return &file{opts: options, path: path}
}
