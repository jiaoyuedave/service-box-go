package memory

import (
	"gitee.com/dennis-kk/service-box-go/util/config/source"
	"time"
)

type (
	memory struct {
		//changeSet is the config data of source
		changeSet *source.ChangeSet
	}
)

func (m *memory) Read() (*source.ChangeSet, error) {
	cs := &source.ChangeSet{
		Data:      m.changeSet.Data,
		Format:    m.changeSet.Format,
		Source:    m.changeSet.Source,
		Timestamp: m.changeSet.Timestamp,
	}
	return cs, nil
}

func (m *memory) String() string {
	return "memory"
}

//NewSource will return memory source with config
func NewSource(opts ...source.Option) source.Source {
	option := source.NewOptions(opts...)

	ms := &memory{}

	if option.Context != nil {
		ms.changeSet = option.Context.Value(memorySourceKey{}).(*source.ChangeSet)
		ms.changeSet.Source = "memory"
		ms.changeSet.Timestamp = time.Now()
		ms.changeSet.Checksum = ms.changeSet.Sum()
	}

	return ms
}
