package memory

import (
	"context"
	"gitee.com/dennis-kk/service-box-go/util/config/source"
)

type (
	memorySourceKey struct{}
)

//WithData will set binary data to source
func WithData(data []byte, format string) source.Option {
	return func(option *source.Options) {
		if option.Context == nil {
			option.Context = context.Background()
		}

		cs := &source.ChangeSet{
			Data:   data,
			Format: format,
		}

		option.Context = context.WithValue(option.Context, memorySourceKey{}, cs)
	}
}

//WithYaml will set yaml file to source
func WithYaml(data []byte) source.Option {
	return WithData(data, "yaml")
}
