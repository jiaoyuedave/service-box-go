package source

import (
	"crypto/md5"
	"fmt"
	"time"
)

// Source is the source from which config is loaded
type Source interface {
	Read() (*ChangeSet, error) //read from source, to change set
	String() string
}

// ChangeSet represents a set of changes from a source
type ChangeSet struct {
	Data      []byte    //binary data
	Checksum  string    //md5 sum
	Format    string    //format
	Source    string    //name
	Timestamp time.Time //time stamp
}

// Sum returns the md5 checksum of the ChangeSet data
func (c *ChangeSet) Sum() string {
	h := md5.New()
	h.Write(c.Data)
	return fmt.Sprintf("%x", h.Sum(nil))
}
