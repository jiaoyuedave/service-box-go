package config

import (
	"gitee.com/dennis-kk/service-box-go/util/config/reader"
	"time"
)

type defaultValue struct{}

func (v *defaultValue) Get(key string) reader.Value {
	return &defaultValue{}
}

func newValue() reader.Value {
	return new(defaultValue)
}

func (v *defaultValue) IsNil() bool {
	return true
}

func (v *defaultValue) Bool(def bool) bool {
	return false
}

func (v *defaultValue) Int(def int) int {
	return 0
}

func (v *defaultValue) String(def string) string {
	return ""
}

func (v *defaultValue) Float64(def float64) float64 {
	return 0.0
}

func (v *defaultValue) Duration(def time.Duration) time.Duration {
	return time.Duration(0)
}

func (v *defaultValue) StringSlice(def []string) []string {
	return nil
}

func (v *defaultValue) StringMap(def map[string]string) map[string]string {
	return map[string]string{}
}

func (v *defaultValue) Scan(val interface{}) error {
	return nil
}

func (v *defaultValue) Bytes() []byte {
	return nil
}
