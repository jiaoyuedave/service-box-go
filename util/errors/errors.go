package errors

import "errors"

var (
	FindServiceTimeOut     = errors.New("find service time out ")
	ChannelNotFound        = errors.New("connection not found")
	ChannelHasClosed       = errors.New("connection has been closed ")
	ChannelNotEnough       = errors.New("not have enough data")
	ChannelRepeatedConnect = errors.New("repeated connect to some host")
	InvalidApolloCfg       = errors.New("Invalid Apollo Config! ")
	ConnectApolloFailed    = errors.New("generate connect Apollo request fail")
	ApolloOverMaxRetry     = errors.New("over Max Retry Still Error")
	RedisNotInit           = errors.New("redis not init")
	RedisConfigError       = errors.New("invalid redis config")
	MongoConfigError       = errors.New("invalid mongo config")
	ProxyConfigError       = errors.New("invalid proxy config")
	HostIpError            = errors.New("invalid listener ip")
	RepeatedWatcher        = errors.New("repeated watcher")
	InvalidWatcherManager  = errors.New("invalid watcher manager")
	InvalidZKConn          = errors.New("invalid zookeeper connection")
)
