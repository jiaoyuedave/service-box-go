package http_proxy

import (
	"github.com/gin-gonic/gin"
)

type (
	//Context http_proxy 上下文，对外屏蔽内部使用的框架细节，便于之后重构和修改
	Context struct {
		ctx *gin.Context
	}
)

func (c *Context) GetHeader(key string) string {
	return c.ctx.GetHeader(key)
}

func (c *Context) GetRawData() ([]byte, error) {
	return c.ctx.GetRawData()
}

func (c *Context) Set(key string, value interface{}) {
	c.ctx.Set(key, value)
}

func (c *Context) SetErr(err error) {
	_ = c.ctx.Error(err)
}
