package http_proxy

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

func customLogFormatter(param gin.LogFormatterParams) string {
	// [GET] PATH PROTO STATUS AGENT ERROR MESSAGE
	return fmt.Sprintf("[%q] %q %q %d %q %q", param.Method, param.Path, param.Request.Proto, param.StatusCode, param.Request.UserAgent(), param.ErrorMessage)
}
