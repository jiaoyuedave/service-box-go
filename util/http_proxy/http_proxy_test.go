package http_proxy

import (
	"bytes"
	"errors"
	"fmt"
	"gitee.com/dennis-kk/service-box-go/example/testcaller"
	"io/ioutil"
	"net/http"
	"os"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/protocol"
	"gitee.com/dennis-kk/service-box-go/example/idldata/pbdata"
	"gitee.com/dennis-kk/service-box-go/example/testcallee"
	_ "gitee.com/dennis-kk/service-box-go/internal/jsonpb/jsonpbcpp"
	"gitee.com/dennis-kk/service-box-go/util/slog"
	"gitee.com/dennis-kk/service-box-go/util/slog/zap"
	"github.com/golang/protobuf/proto"
)

var (
	logger slog.BoxLogger
)

type (
	//testHttpService 测试服务器
	testHttpServer struct {
		callID    uint32
		err       error
		hp        *HttpProxy                   // hp 示例
		sg        chan struct{}                // 关闭通道
		wg        sync.WaitGroup               // 关闭处理
		frame     int32                        // 帧率，单位毫秒
		respQueue []*protocol.ProxyRespPackage // 需要回包的队列
	}
)

func init() {
	logger, _ = zap.NewLogger(slog.WithCallerSkipCount(1), slog.WithLevel(slog.LogDebug))
}

func testNewHttpServer(opts ...Option) *testHttpServer {

	defer func() {
		if r := recover(); r != nil {
			return
		}
	}()

	hp := MakeHttpProxy(opts...)

	hs := &testHttpServer{
		callID:    1,
		hp:        hp,
		wg:        sync.WaitGroup{},
		sg:        make(chan struct{}, 1),
		frame:     10,
		respQueue: make([]*protocol.ProxyRespPackage, 0, 48),
	}

	return hs
}

func (hs *testHttpServer) start() error {

	// 启动 http server
	err := hs.hp.Start()
	if err != nil {
		return err
	}

	// 启动逻辑主循环
	start := make(chan struct{})
	hs.wg.Add(1)
	go func() {
		start <- struct{}{}
		defer hs.wg.Done()

		timer := time.NewTimer(time.Duration(hs.frame) * time.Millisecond)
		for {
			select {
			case <-timer.C:
				hs.hp.Tick()
				// 回复 所有请求
				hs.dealResp()
				timer.Reset(time.Duration(hs.frame) * time.Millisecond)
			case <-hs.sg:
				return
			}
		}
	}()

	<-start
	return nil
}

func (hs *testHttpServer) addResp(callId uint32, buffer []byte, errorCode uint32) {

	resp := &protocol.ProxyRespPackage{
		Header: &protocol.RpcProxyCallRetHeader{
			CallID:    callId,
			ErrorCode: errorCode,
		},
		Buffer: buffer,
	}
	hs.respQueue = append(hs.respQueue, resp)
}

func (hs *testHttpServer) dealResp() {
	for _, resp := range hs.respQueue {
		err := hs.hp.OnRpcResponse(resp)
		if err != nil {
			hs.err = err
			continue
		}
	}

	hs.respQueue = hs.respQueue[:0]
}

func (hs *testHttpServer) shutdown() {
	hs.hp.ShutDown()
	hs.sg <- struct{}{}
	hs.wg.Wait()
}

func (hs *testHttpServer) genID() uint32 {
	return atomic.AddUint32(&hs.callID, 1)
}

func TestHttpProxy_Init(t *testing.T) {
	hs := testNewHttpServer(
		func(option *Options) {
			option.Address = "0.0.0.0:8080"
			option.ServicePath = "../../example/idl"
			option.Mode = "debug"
			option.ShutdownTimeout = 5000
			option.CacheSize = 512
		},
		WithCustomLogger(logger),
	)

	if hs == nil {
		t.Fatal("init error !")
	}
}

func TestHttpProxy_InitWithInvalidPath(t *testing.T) {
	hs := testNewHttpServer(func(option *Options) {
		option.Address = "0.0.0.0:8080"
		option.ServicePath = "/foo/path"
		option.Mode = "debug"
		option.ShutdownTimeout = 0
		option.logger = logger
	})

	if hs != nil {
		t.Fatal("init error !")
	}

}

func TestHttpProxy_InitWithInvalidAddress(t *testing.T) {
	hs := testNewHttpServer(func(option *Options) {
		option.Address = ""
		option.ServicePath = "/foo/path"
		option.Mode = "debug"
		option.ShutdownTimeout = 0
		option.logger = logger
	})

	if hs != nil {
		t.Fatal("init error !")
	}

	hs = testNewHttpServer(func(option *Options) {
		option.Address = "0.0.0.0"
		option.ServicePath = "../../example/idl"
		option.Mode = "debug"
		option.ShutdownTimeout = 0
		option.logger = logger
	})

	if hs != nil {
		t.Fatal("init error !")
	}
}

func TestHttpProxy_Start(t *testing.T) {
	hs := testNewHttpServer(func(option *Options) {
		option.Address = "0.0.0.0:8080"
		option.ServicePath = "../../example/idl"
		option.Mode = "debug"
		option.ShutdownTimeout = 5000
		option.logger = logger
	})

	if hs == nil {
		t.Fatal("init error !")
	}

	// 启动
	err := hs.start()
	if err != nil {
		t.Fatal(err)
	}

	// 检查 http 端口是否正确开放
	client := &http.Client{}
	req, err := http.NewRequest("POST", "http://0.0.0.0:8080/", nil)
	if err != nil {
		t.Fatal(err)
	}

	resp, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	if resp.StatusCode != http.StatusNotFound {
		t.Fatalf("un excepted http response code %d", resp.StatusCode)
	}

	// 主流程 shutdown
	hs.shutdown()
}

// TestHttpProxy_CalHttpMethod 测试调用http方法
func TestHttpProxy_CalHttpMethod(t *testing.T) {
	hs := testNewHttpServer(func(option *Options) {
		option.Address = "0.0.0.0:8080"
		option.ServicePath = "../../example/idl"
		option.Mode = "debug"
		option.ShutdownTimeout = 5000
		option.logger = logger
	})

	if hs == nil {
		t.Fatal("init error !")
	}

	requestHandle := func(req *protocol.ProxyRequestPackage) (uint32, error) {
		// 检查服务id 是否正确
		if req.Header.ServiceUUID != testcallee.SrvUUID {
			t.Fatalf("unexcepted service uuid %d -- %d", req.Header.ServiceUUID, testcallee.SrvUUID)
		}

		// 检查方法id 是否正确
		if req.Header.MethodID != 1 {
			t.Fatalf("unexcepted service method %d", req.Header.MethodID)
		}

		// 检查序列化数据是否正确
		reqPb := &pbdata.TestCallee_AddArgs{}
		err := proto.Unmarshal(req.Buffer, reqPb)
		if err != nil {
			t.Fatalf("unexcepted rpc request %q", err.Error())
		}

		// 检查参数是否正确
		if reqPb.Arg1 != 233 || reqPb.Arg2 != 466 {
			t.Fatalf("unexcepted request prase %d  %d", reqPb.Arg1, reqPb.Arg2)
		}

		callId := hs.genID()

		//回包测试
		resp := &pbdata.TestCallee_AddRet{Ret1: 599}
		buffer, _ := proto.Marshal(resp)
		hs.addResp(callId, buffer, protocol.IDL_SUCCESS)

		return callId, nil
	}

	hs.hp.SetHttpRequestHandle(requestHandle)

	// 启动服务器
	if err := hs.start(); err != nil {
		t.Fatal(err)
	}

	// 发送请求
	client := &http.Client{}
	jsonBody := []byte(`{"args": {"arg1": 233, "arg2":466}}`)
	bodyReader := bytes.NewReader(jsonBody)
	req, err := http.NewRequest("POST", "http://0.0.0.0:8080/TestCallee/Add", bodyReader)
	if err != nil {
		t.Fatal(err)
	}

	resp, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	if resp == nil {
		t.Fatal("no effective response")
	}

	body, _ := ioutil.ReadAll(resp.Body)
	// 检查是否被调用过
	if hs.callID != 2 {

		t.Fatalf("unexcepted call count %d %s", hs.callID, body)
	} else {
		t.Logf("%s", body)
	}

	hs.shutdown()
}

func TestHttpProxy_CallMethodCase(t *testing.T) {
	callCase := []struct {
		uri       string
		body      []byte
		expStatus int
		expBody   string
		expErr    string
	}{
		{
			"http://0.0.0.0:8080/TestCallee/Add",
			[]byte(`{"args": {"arg1": 233, "arg2":466}}`),
			http.StatusOK,
			"\"{\\n\\t\\\"ret1\\\" : 599\\n}\\n\"",
			"",
		},
		// 测试空body
		{
			"http://0.0.0.0:8080/TestCallee/Add",
			[]byte(""),
			http.StatusBadRequest,
			"{\"message\":\"Invalid parameters length\"}",
			"",
		},
		// 测试非法的json格式
		{
			"http://0.0.0.0:8080/TestCallee/Add",
			[]byte("plato foo test"),
			http.StatusBadRequest,
			`{"error":"Invalid JSON format"}`,
			"",
		},
		// 测试参数不全
		{
			"http://0.0.0.0:8080/TestCallee/Add",
			[]byte(`{"args": {"arg1": 233}}`),
			http.StatusBadRequest,
			`{"error":"Invalid argument number, need 2 but 1given"}`,
			"",
		},
		// 测试参数名错误，但是数量正确
		{
			"http://0.0.0.0:8080/TestCallee/Add",
			[]byte(`{"args": {"arg1": 233, "arg3":466}}`),
			http.StatusBadRequest,
			`{"error":"Method argument name invalid"}`,
			"",
		},
	}

	hs := testNewHttpServer(func(option *Options) {
		option.Address = "0.0.0.0:8080"
		option.ServicePath = "../../example/idl"
		option.Mode = "debug"
		option.ShutdownTimeout = 5000
		option.logger = logger
	})

	if hs == nil {
		t.Fatal("init error !")
	}

	requestHandle := func(req *protocol.ProxyRequestPackage) (uint32, error) {
		callId := hs.genID()
		//回包测试
		resp := &pbdata.TestCallee_AddRet{Ret1: 599}
		buffer, _ := proto.Marshal(resp)
		hs.addResp(callId, buffer, protocol.IDL_SUCCESS)

		return callId, nil
	}

	hs.hp.SetHttpRequestHandle(requestHandle)

	// 启动服务器
	if err := hs.start(); err != nil {
		t.Fatal(err)
	}

	for _, one := range callCase {
		client := &http.Client{}
		var bodyReader *bytes.Reader
		if one.body != nil {
			bodyReader = bytes.NewReader(one.body)
		} else {
			bodyReader = nil
		}

		req, err := http.NewRequest("POST", one.uri, bodyReader)
		if err != nil {
			t.Fatal(err)
		}

		resp, err := client.Do(req)
		if resp == nil {
			t.Fatalf("no effective response %s", err)
		}

		if resp.StatusCode != one.expStatus {
			t.Fatalf("unexcepted response status %d:%d", resp.StatusCode, one.expStatus)
		}

		body, _ := ioutil.ReadAll(resp.Body)
		if string(body[:]) != one.expBody {
			t.Fatalf("unexception response body uri:%s param:%s %s -- %s", one.uri, one.body, body, one.expBody)
		}
	}

	hs.shutdown()
}

// 模拟消息过度，队列中消息超时的情况
func TestHttpProxy_RequestTimeout(t *testing.T) {
	hs := testNewHttpServer(func(option *Options) {
		option.Address = "0.0.0.0:8080"
		option.ServicePath = "../../example/idl"
		option.Mode = "debug"
		option.ShutdownTimeout = 5000
		option.logger = logger
	})

	if hs == nil {
		t.Fatal("init error !")
	}

	requestHandle := func(req *protocol.ProxyRequestPackage) (uint32, error) {
		callId := hs.genID()

		//回包测试
		resp := &pbdata.TestCallee_AddRet{Ret1: 599}
		buffer, _ := proto.Marshal(resp)
		hs.addResp(callId, buffer, protocol.IDL_SUCCESS)

		return callId, nil
	}

	hs.frame = 1000

	hs.hp.SetHttpRequestHandle(requestHandle)

	// 启动服务器
	if err := hs.start(); err != nil {
		t.Fatal(err)
	}

	// 发送请求
	client := &http.Client{}
	jsonBody := []byte(`{"args": {"arg1": 233, "arg2":466}}`)
	wg := sync.WaitGroup{}
	goFunc := func() {

		defer wg.Done()

		bodyReader := bytes.NewReader(jsonBody)
		req, err := http.NewRequest("POST", "http://0.0.0.0:8080/TestCallee/Add", bodyReader)
		if err != nil {
			t.Fatal(err)
		}

		resp, err := client.Do(req)
		if err != nil {
			t.Fatal(err)
		}

		if resp == nil {
			t.Fatal("no effective response")
		}

		if resp.StatusCode != http.StatusGatewayTimeout {
			t.Fatalf("unexception status %d", resp.StatusCode)
		}
	}

	wg.Add(2)
	go goFunc()
	go goFunc()

	wg.Wait()

	hs.shutdown()
}

// 测试各种非法response 的情况
func TestHttpProxy_OnRpcResponse(t *testing.T) {
	hs := testNewHttpServer(func(option *Options) {
		option.Address = "0.0.0.0:8080"
		option.ServicePath = "../../example/idl"
		option.Mode = "debug"
		option.ShutdownTimeout = 5000
		option.logger = logger
	})

	if hs == nil {
		t.Fatal("init error !")
	}

	requestHandle := func(req *protocol.ProxyRequestPackage) (uint32, error) {
		callId := hs.genID()
		// 根据 callId 构造各种回包情况
		switch callId {
		case 2:
			// 不完整参数
			resp := &pbdata.TestCallee_AddRet{}
			buffer, _ := proto.Marshal(resp)
			hs.addResp(callId, buffer, protocol.IDL_SUCCESS)
		case 3:
			// 构造参数为空
			hs.addResp(callId, nil, protocol.IDL_SUCCESS)
		case 4:
			// 模拟协议版本不对，协议体对不上
			resp := &pbdata.TestCaller_GetInfoRet{Ret1: "plato test"}
			buffer, _ := proto.Marshal(resp)
			hs.addResp(callId, buffer[:5], protocol.IDL_SUCCESS)
		case 5:
			// 返回rpc超时
			hs.addResp(callId, nil, protocol.IDL_RPC_TIME_OUT)
		case 6:
			// 返回rpc服务未找到
			hs.addResp(callId, nil, protocol.IDL_SERVICE_NOT_FOUND)
		case 7:
			// 测试 返回错误
			return 0, errors.New("test case return error")
		}

		return callId, nil
	}

	hs.hp.SetHttpRequestHandle(requestHandle)

	// 启动服务器
	if err := hs.start(); err != nil {
		t.Fatal(err)
	}

	callCase := []struct {
		exceptStatus int
	}{
		{
			http.StatusOK,
		},
		{
			http.StatusOK,
		},
		{
			http.StatusInternalServerError,
		},
		{
			http.StatusGatewayTimeout,
		},
		{
			http.StatusNotFound,
		},
		{
			http.StatusInternalServerError,
		},
	}

	for _, one := range callCase {
		client := &http.Client{}
		jsonBody := []byte(`{"args": {"arg1": 233, "arg2":466}}`)
		bodyReader := bytes.NewReader(jsonBody)
		req, err := http.NewRequest("POST", "http://0.0.0.0:8080/TestCallee/Add", bodyReader)
		if err != nil {
			t.Fatal(err)
		}
		resp, err := client.Do(req)
		if resp == nil {
			t.Fatalf("no effective response %s", err)
		}

		if resp.StatusCode != one.exceptStatus {
			body, _ := ioutil.ReadAll(resp.Body)
			hs.hp.logger.Info("%s", body)
			t.Errorf("unexception error code %d %d", resp.StatusCode, one.exceptStatus)
		}
	}

	hs.shutdown()
}

func TestHttpProxy_CallProtectedMethod(t *testing.T) {
	hs := testNewHttpServer(func(option *Options) {
		option.Address = "0.0.0.0:8080"
		option.ServicePath = "../../example/idl"
		option.Mode = "debug"
		option.ShutdownTimeout = 5000
		option.logger = logger
	})

	if hs == nil {
		t.Fatal("init error !")
	}

	requestHandle := func(req *protocol.ProxyRequestPackage) (uint32, error) {
		// 检查服务id 是否正确
		if req.Header.ServiceUUID != testcaller.SrvUUID {
			t.Fatalf("unexcepted service uuid %d -- %d", req.Header.ServiceUUID, testcaller.SrvUUID)
		}

		// 检查方法id 是否正确
		if req.Header.MethodID != 1 {
			t.Fatalf("unexcepted service method %d", req.Header.MethodID)
		}

		callId := hs.genID()

		hs.addResp(callId, nil, protocol.IDL_SUCCESS)

		return callId, nil
	}

	hs.hp.SetHttpRequestHandle(requestHandle)

	// 启动服务器
	if err := hs.start(); err != nil {
		t.Fatal(err)
	}

	// 发送请求
	client := &http.Client{}
	jsonBody := []byte(`{"args": {"arg1": "plato"}}`)
	bodyReader := bytes.NewReader(jsonBody)
	req, err := http.NewRequest("POST", "http://0.0.0.0:8080/TestCaller/SetInfo", bodyReader)
	if err != nil {
		t.Fatal(err)
	}

	resp, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	if resp == nil {
		t.Fatal("no effective response")
	}

	if resp.StatusCode != http.StatusOK {
		t.Fatalf("unexcepted status code %d", resp.StatusCode)
	}

	hs.shutdown()
}

func TestHttpProxy_CallProtectedMethodWithAuth(t *testing.T) {
	hs := testNewHttpServer(func(option *Options) {
		option.Address = "0.0.0.0:8080"
		option.ServicePath = "../../example/idl"
		option.Mode = "debug"
		option.ShutdownTimeout = 5000
		option.logger = logger
	})

	if hs == nil {
		t.Fatal("init error !")
	}

	authHandle := func(c *Context) error {
		name := c.GetHeader("name")
		passwd := c.GetHeader("passwd")

		if name == "plato" && passwd == "test" {
			return nil
		} else {
			logger.Info("%s --- %s", name, passwd)
			return fmt.Errorf("invalid user authenticator")
		}
	}

	SetAuthHandle(authHandle)

	requestHandle := func(req *protocol.ProxyRequestPackage) (uint32, error) {
		// 检查服务id 是否正确
		if req.Header.ServiceUUID != testcaller.SrvUUID {
			t.Fatalf("unexcepted service uuid %d -- %d", req.Header.ServiceUUID, testcaller.SrvUUID)
		}

		// 检查方法id 是否正确
		if req.Header.MethodID != 1 {
			t.Fatalf("unexcepted service method %d", req.Header.MethodID)
		}

		callId := hs.genID()

		hs.addResp(callId, nil, protocol.IDL_SUCCESS)

		return callId, nil
	}

	hs.hp.SetHttpRequestHandle(requestHandle)

	// 启动服务器
	if err := hs.start(); err != nil {
		t.Fatal(err)
	}

	testCase := []struct {
		headers   map[string]string
		expStatus int
	}{
		{
			headers: map[string]string{
				"name":   "plato",
				"passwd": "test",
			},
			expStatus: http.StatusOK,
		},
		{
			headers:   nil,
			expStatus: http.StatusForbidden,
		},
	}

	// 发送请求
	client := &http.Client{}
	jsonBody := []byte(`{"args": {"arg1": "plato"}}`)

	for _, one := range testCase {
		bodyReader := bytes.NewReader(jsonBody)
		req, err := http.NewRequest("POST", "http://0.0.0.0:8080/TestCaller/SetInfo", bodyReader)

		if err != nil {
			t.Fatal(err)
		}

		for k, v := range one.headers {
			req.Header.Add(k, v)
		}

		resp, err := client.Do(req)
		if err != nil {
			t.Fatal(err)
		}

		if resp == nil {
			t.Fatal("no effective response")
		}

		if resp.StatusCode != one.expStatus {
			body, _ := ioutil.ReadAll(resp.Body)
			t.Fatalf("unexcepted status code %d %s", resp.StatusCode, body)
		}
	}

	hs.shutdown()
}

func TestHttpProxy_OnServiceChange(t *testing.T) {
	hs := testNewHttpServer(func(option *Options) {
		option.Address = "0.0.0.0:8080"
		option.ServicePath = "../../example/idl"
		option.Mode = "debug"
		option.ShutdownTimeout = 5000
		option.logger = logger
	})

	if hs == nil {
		t.Fatal("init error !")
	}

	requestHandle := func(req *protocol.ProxyRequestPackage) (uint32, error) {
		callId := hs.genID()
		// 根据 callId 构造各种回包情况
		hs.addResp(callId, nil, protocol.IDL_SUCCESS)
		return callId, nil
	}

	hs.hp.SetHttpRequestHandle(requestHandle)

	// 启动
	err := hs.start()
	if err != nil {
		t.Fatal(err)
	}

	// 添加文件
	os.Rename("../../example/sub", "../../example/idl/sub")

	defer os.Rename("../../example/idl/sub", "../../example/sub")

	// sleep 触发tick
	time.Sleep(50 * time.Millisecond)

	client := &http.Client{}
	jsonBody := []byte(`{"args": {"name":"plato test" }}`)
	bodyReader := bytes.NewReader(jsonBody)
	req, err := http.NewRequest("POST", "http://0.0.0.0:8080/TestCase1/Hello", bodyReader)
	if err != nil {
		t.Fatal(err)
	}
	resp, err := client.Do(req)
	if resp == nil {
		t.Fatalf("no effective response %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		body, _ := ioutil.ReadAll(resp.Body)
		t.Fatalf("unexcepted status %d error %s", resp.StatusCode, body)
	}
	hs.shutdown()
}

func BenchmarkHttpProxy_OnRpcResponse(b *testing.B) {
	hs := testNewHttpServer(func(option *Options) {
		option.Address = "0.0.0.0:8080"
		option.ServicePath = "../../example/idl"
		option.Mode = "release"
		option.ShutdownTimeout = 5000
		option.CacheSize = 512
		option.logger = logger
	})

	if hs == nil {
		b.Fatal("init error !")
	}

	requestHandle := func(req *protocol.ProxyRequestPackage) (uint32, error) {
		// 检查服务id 是否正确
		callId := hs.genID()
		//回包测试
		resp := &pbdata.TestCallee_AddRet{Ret1: 599}
		buffer, _ := proto.Marshal(resp)
		hs.addResp(callId, buffer, protocol.IDL_SUCCESS)

		return callId, nil
	}

	hs.hp.SetHttpRequestHandle(requestHandle)

	// 启动
	err := hs.start()
	if err != nil {
		b.Fatal(err)
	}

	jsonBody := []byte(`{"args": {"arg1": 233, "arg2":466}}`)
	bodyReader := bytes.NewReader(jsonBody)

	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			client := &http.Client{}
			req, err := http.NewRequest("POST", "http://0.0.0.0:8080/TestCallee/Add", bodyReader)
			if err != nil {
				b.Fatal(err)
			}
			client.Do(req)
		}
	})

	hs.shutdown()
}
