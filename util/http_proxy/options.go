package http_proxy

import "gitee.com/dennis-kk/service-box-go/util/slog"

type (
	//Authenticator 鉴权函数Handle
	Authenticator func(ctx *Context) error

	// Options http proxy 模块配置
	Options struct {
		Address         string         `yaml:"address"`          //监听地址
		Auth            string         `yaml:"auth"`             //鉴权类型，留空不开启鉴权
		ServicePath     string         `yaml:"service_path"`     //服务配置路径
		Mode            string         `yaml:"mode"`             // 运行模式 debug release
		ShutdownTimeout uint32         `yaml:"shutdown_timeout"` //停机超时时间, 单位毫秒
		CacheSize       uint32         `yaml:"cache_size"`       // 命令排队数量，默认是1024，框架处理速率是每秒 5000 条命令左右 不应该有太多堆积
		logger          slog.BoxLogger // 日志接口
	}
	// Option 设置函数
	Option func(options *Options)
)

var (
	//authenticator 鉴权中间件
	authenticator Authenticator
)

func SetAuthHandle(auth Authenticator) {
	authenticator = auth
}

func WithCustomLogger(logger slog.BoxLogger) Option {
	return func(op *Options) {
		op.logger = logger
	}
}
