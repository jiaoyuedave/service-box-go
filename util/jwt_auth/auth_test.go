package jwt_auth

import (
	"fmt"
	"testing"
	"time"
)

var (
	option *Options
)

type (
	testPayload struct {
		Uid  uint64 `json:"uid"`
		Name string `json:"name"`
		Exp  int64  `json:"exp"`
		Iat  int64  `json:"iat"`
	}
)

func init() {
	option = &Options{
		Enable:     true,
		AuthHeader: "Authorization",
		TokenType:  "Bearer",
		Encryption: "HS256",
		Secret:     "hello plato",
	}
}

func (t *testPayload) Valid() error {

	if t.Exp < time.Now().Unix()+60 {
		return fmt.Errorf("token expired ")
	}

	return nil
}

func TestJwtAuth_GenTokenWithPayload(t *testing.T) {
	testJwt := &JwtAuth{
		opt: option,
	}

	testCases := []struct {
		payload *testPayload
		expStr  string
		expErr  error
	}{
		{
			&testPayload{
				1233,
				"plato",
				233333333,
				0,
			},
			"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjEyMzMsIm5hbWUiOiJwbGF0byIsImV4cCI6MjMzMzMzMzMzLCJpYXQiOjB9.E8TgkpMDza-yq7GQ0EhE9lvevE7ZFt-AAnPKjfrjHcM",
			nil,
		},
		// 测试 空payload
		{
			nil,
			"",
			ErrInvalidPayload,
		},
	}

	for _, one := range testCases {
		var jwtToken string
		var err error
		if one.payload == nil {
			jwtToken, err = testJwt.GenTokenWithPayload(nil)
		} else {
			jwtToken, err = testJwt.GenTokenWithPayload(one.payload)
		}

		if err != one.expErr {
			t.Fatalf("unexcepted GenTokenWithPayload err %v, %v", err, one.expErr)
		}

		if jwtToken != one.expStr {
			t.Fatalf("unexcepted GenTokenWithPayload str %q : %q", jwtToken, one.expStr)
		}
	}

}

func TestJwtAuth_GetTokenWithClaims(t *testing.T) {
	testJwt := &JwtAuth{
		opt: option,
	}

	testCases := []struct {
		claims map[string]interface{}
		expStr string
		expErr error
	}{
		{
			map[string]interface{}{
				"exp":  2333,
				"name": "plato",
				"id":   123,
			},
			"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjIzMzMsImlkIjoxMjMsIm5hbWUiOiJwbGF0byJ9.S75FIwYqYfj4496Dvg1MwPH2phRPmE4uLzZHvHcNcxI",
			nil,
		},
		// 测试 空payload
		{
			nil,
			"",
			ErrInvalidPayload,
		},
	}

	for _, one := range testCases {
		jwtToken, err := testJwt.GenTokenWithClaims(one.claims)
		if err != one.expErr {
			t.Fatalf("unexcepted GenTokenWithPayload err %v, %v", err, one.expErr)
		}

		if jwtToken != one.expStr {
			t.Fatalf("unexcepted GenTokenWithPayload str %q : %q", jwtToken, one.expStr)
		}
	}
}

func TestJwtAuth_GetToken(t *testing.T) {
	testJwt := &JwtAuth{
		opt: option,
	}
	tokenStr := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTIzLCJuYW1lIjoicGxhdG8iLCJpdGEiOjEuNDU2NzgsImV4cCI6MTY3MDkxMzI3Nn0.LxcuuIz9Mt4u0lLYZHObENWDNsLbe0gfgl76JYZxfVc"
	tokenMap, err := testJwt.GetToken(tokenStr)
	if err == nil {
		t.Fatalf("unexcepted token error ")
	}

	tokenStr = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTIzLCJuYW1lIjoicGxhdG8iLCJpdGEiOjEuNDU2NzgsImV4cCI6MTY4MDkxMzI3Nn0.oLmYBbJORo-yOEOBZ7-ch-NaQmoefdgNBisQO-doCeI"
	tokenMap, err = testJwt.GetToken(tokenStr)
	if err != nil {
		t.Fatalf("unexcepted token error %s", err.Error())
	}
	if len(tokenMap) != 4 {
		t.Fatalf("unexcepted token's length")
	}
}

func TestJwtAuth_GetPayloadFromToken(t *testing.T) {
	tp := &testPayload{}
	testJwt := &JwtAuth{
		opt: option,
	}

	tokenStr := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjEyMzMsIm5hbWUiOiJwbGF0byIsImV4cCI6MTY4MDkxMzU3NSwiaWF0IjoyMzMzfQ.HF0MxYmuRw90wuBN97kz4sDAWv6rBU_74BLNhzR_Tvg"
	err := testJwt.GetPayloadFromToken(tokenStr, tp)
	if err != nil {
		t.Fatalf("unexcepted token error %s", err.Error())
	}
	if tp.Name != "plato" {
		t.Fatalf("unexcepted payload's properties ")
	}
}
