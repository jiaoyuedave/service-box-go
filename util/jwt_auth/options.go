package jwt_auth

type (
	Options struct {
		// Enable 是否启用Auth模块
		Enable bool `yaml:"enable"`
		//AuthHeader http头部中用于鉴权的字段名
		AuthHeader string `yaml:"auth_header"`
		//TokenType token 类型 如 Bearer
		TokenType string `yaml:"token_type"`
		//TokenExpires token 过期时间
		TokenExpiresTime int `yaml:"token_expires_time"`
		//TokenRefreshTime token 刷新续期时间
		TokenRefreshTime int `yaml:"token_refresh_time"`
		//Encryption 编码方式，如HS256
		Encryption string `yaml:"encryption"`
		//Secret 密钥
		Secret string `yaml:"secret"`
	}
	Option func(options *Options)
)
