package mongodb

import (
	"testing"
)

var (
	testSingleNode = &Options{
		DB: "plato",
		Mongos: []string{
			"127.0.0.1:30000",
		},
	}

	testMutilNode = &Options{
		DB: "plato",
		Mongos: []string{
			"127.0.0.1:30000",
			"127.0.0.2:30001",
		},
	}

	testSingleAuth = &Options{
		User:     "plato",
		Password: "test@#233",
		DB:       "plato",
		Mongos: []string{
			"127.0.0.1:30000",
		},
	}

	testMutilAuth = &Options{
		User:     "plato",
		Password: "test@#233",
		DB:       "plato",
		Mongos: []string{
			"127.0.0.1:30000",
			"127.0.0.2:30001",
		},
	}
)

func TestGenMongoUri(t *testing.T) {
	testCase := []struct {
		opt       *Options
		exceptUrl string
		exceptErr error
	}{
		{
			testSingleNode,
			"mongodb://127.0.0.1:30000/plato",
			nil,
		},
		{
			testMutilNode,
			"mongodb://127.0.0.1:30000,127.0.0.2:30001/plato",
			nil,
		},
		{
			testSingleAuth,
			"mongodb://plato:test@#233@127.0.0.1:30000/plato?authSource=plato&authMechanism=SCRAM-SHA-1",
			nil,
		},
		{
			testMutilAuth,
			"mongodb://plato:test@#233@127.0.0.1:30000,127.0.0.2:30001/plato?authSource=plato&authMechanism=SCRAM-SHA-1",
			nil,
		},
	}

	for _, c := range testCase {
		uri, err := c.opt.genMongoUri()
		if uri != c.exceptUrl || err != c.exceptErr {
			t.Errorf("unexcpet result %s %v : %s %v", uri, err, c.exceptUrl, c.exceptErr)
		}
	}

}

func TestNewMongoDB(t *testing.T) {
	client := NewMongoDB(func(opts *Options) {
		opts.User = "plato"
		opts.Password = "test"
		opts.DB = "test"
		opts.Mongos = []string{
			"127.0.0.1:30000",
		}
	})

	err := client.Init()
	if err != nil {
		t.Fatal(err)
	}

	err = client.Start()
	if err != nil {
		t.Fatal(err)
	}
}
