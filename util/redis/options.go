package redis

import "gopkg.in/yaml.v3"

type (
	//Options redis 配置
	Options struct {
		// AddrHosts 集群节点 非集群模式配置一个 集群模式为一组， ip:port
		AddrHosts []string `yaml:"addr_hosts"`
		//UserName 鉴权使用的用户名
		UserName string `yaml:"user_name"`
		//Password 鉴权
		Password string `yaml:"password"`
		// DB 控制选择的数据库 默认 0
		DB int `yaml:"db"`
		// PoolSize 控制与redis 连接的最大连接数 默认为10
		PoolSize int `yaml:"pool_size"`
		// ReadOnly slave 节点启用 read-only 命令 集群模式生效
		ReadOnly bool `yaml:"read_only"`
	}
	Option func(o *Options)
)

//WithHostNode 追加节点到配置
func WithHostNode(hosts ...string) Option {
	return func(o *Options) {
		if o == nil {
			return
		}
		o.AddrHosts = append(o.AddrHosts, hosts...)
	}
}

//WithAuth 设置鉴权信息
func WithAuth(passwd string) Option {
	return func(o *Options) {
		if o == nil {
			return
		}

		o.Password = passwd
	}
}

//WithDict 从字典中设置配置到Options
func WithDict(cfg map[string]interface{}) Option {
	return func(o *Options) {
		if o == nil {
			return
		}

		b, err := yaml.Marshal(cfg)
		if err != nil {
			panic(err)
		}

		err = yaml.Unmarshal(b, o)
		if err != nil {
			panic(err)
		}
	}
}
