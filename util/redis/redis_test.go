package redis

import (
	"gopkg.in/yaml.v3"
	"testing"
)

var (
	singleCfg = map[string]interface{}{
		"addr_hosts": []string{
			"127.0.0.21:6379",
		},
	}

	clusterCfg = map[string]interface{}{
		"addr_hosts": []string{
			"127.0.0.17:6379",
			"127.0.0.18:6379",
			"127.0.0.19:6379",
			"127.0.0.17:6380",
			"127.0.0.18:6380",
			"127.0.0.19:6380",
		},
		"password": "test_case",
	}

	testClient  *client      //测试用客户端
	testCluster *slaveClient //测试用集群客户端
)

func testInitClient() error {
	testClient = NewClient(WithDict(singleCfg)).(*client)
	if err := testClient.Init(); err != nil {
		return err
	}

	if err := testClient.Start(); err != nil {
		return err
	}
	return nil
}

func testInitCluster() error {
	testCluster = NewClusterClient(WithDict(clusterCfg)).(*slaveClient)
	if err := testCluster.Init(); err != nil {
		return err
	}

	if err := testCluster.Start(); err != nil {
		return err
	}
	return nil
}

func TestWithConfig(t *testing.T) {

	marshal, err := yaml.Marshal(singleCfg)
	if err != nil {
		return
	}

	config := Options{}
	err = yaml.Unmarshal(marshal, &config)
	if err != nil {
		return
	}
	if len(config.AddrHosts) != 1 {
		t.Fatal("Config Dict Init error")
	}
}

func TestNewClient(t *testing.T) {
	if err := testInitClient(); err != nil {
		t.Fatal(err)
	}

	err := testClient.UnInit()
	if err != nil {
		t.Fatal(err)
		return
	}
}

func TestNewClusterClient(t *testing.T) {
	if err := testInitCluster(); err != nil {
		t.Fatal(err)
	}

	err := testCluster.UnInit()
	if err != nil {
		return
	}
}
