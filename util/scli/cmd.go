package scli

type (
	//Cmd is box's command line parser
	Cmd interface {
		//Init will construct command line parser with options
		Init(options ...Option) error
		//Parse will parse so.args
		Parse([]string) error
		//GetString will get string flag from cli context
		GetString(string) string
		//GetBool will get bool flag from cli context
		GetBool(string) bool
		//GetNumber will get number flag from cli context
		GetNumber(string) int
	}
)

func NewCmd(opts ...Option) Cmd {
	return newCmd(opts...)
}
