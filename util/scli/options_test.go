package scli

import "testing"

type (
	testGeneric struct {
		value string
	}
)

func (t *testGeneric) Set(value string) error {
	t.value = value
	return nil
}

func (t *testGeneric) String() string {
	return t.value
}

func TestWithGenericFlag(t *testing.T) {
	g := testGeneric{}
	flag := &FlagCfg{
		BaseCfg: &BaseCfg{
			Name:  "test",
			Usage: "TestWithFlag",
		},
		Dest: &g,
	}

	opt := WithFlag(flag)
	cmd := NewCmd(opt)
	err := cmd.Parse([]string{"testapp", "--test", "test1 test2 test3"})
	if err != nil {
		t.Fatal(err)
		return
	}

	if g.value != "test1 test2 test3" {
		t.Fatalf("unexception value %s", g.value)
	}
}

func TestWithNoGeneric(t *testing.T) {
	g := testGeneric{}
	flag := &FlagCfg{
		BaseCfg: &BaseCfg{
			Name:  "test",
			Usage: "TestWithFlag",
		},
		Dest: &g,
	}

	opt := WithFlag(flag)
	cmd := NewCmd(opt)
	err := cmd.Parse([]string{"testapp"})
	if err != nil {
		t.Fatal(err)
		return
	}
}
