package service_infra

type (
	//ChangedType service info type
	ChangedType int

	//ChangedInfo service changed info
	ChangedInfo struct {
		Hosts []string
	}

	//ServiceEventHandle service changed handler
	ServiceEventHandle func(svsName string, eventType ChangedType, changeInfo *ChangedInfo, err error)

	//ServiceInfo service info
	ServiceInfo struct {
		Hosts []string // ip:port
	}

	// IServiceInfra service finder and register interface
	IServiceInfra interface {
		// Init will init service infra module with custom option
		Init(...Option) error
		// Start service-infra module, connect to soa center
		Start() error
		// Stop disconnect to soa center, remove all service cache
		Stop() error
		// Tick tick func called by logic goroutine
		Tick()
		//FindServiceAsync will find service asynchronous
		//It will call callback function which added by AddListener
		FindServiceAsync(svsName string) error
		// FindService will find service from soa system by name
		FindService(svsName string) (*ServiceInfo, error)
		// RegisterService register service to soa
		RegisterService(svsName string, info *ServiceInfo) error
		// UnRegisterService unregister service
		UnRegisterService(svsName string, info *ServiceInfo) error
		//AddListener will store service changed callback by service name
		AddListener(handle ServiceEventHandle) error
		//DelListener del server event listener
		DelListener() error
		//IsConnected is connected  to soa center
		IsConnected() bool
	}
)

const (
	ServiceAdd ChangedType = iota
	ServiceChange
	ServiceDelete
)

// MakeServiceInfo create service info with service list
func MakeServiceInfo(hosts []string) *ServiceInfo {
	return &ServiceInfo{
		hosts,
	}
}
