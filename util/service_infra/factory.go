package service_infra

import "sync"

type infraCreator func() IServiceInfra
type infraFactory map[string]infraCreator

var (
	once           sync.Once
	infraCreatoMap infraFactory
)

func init() {
	once.Do(func() {
		infraCreatoMap = make(infraFactory)
	})
}

func RegisterServiceInfra(name string, creator infraCreator) {
	if infraCreatoMap == nil {
		//TODO add error log
		return
	}
	infraCreatoMap[name] = creator
	return
}

func CreateServiceInfra(name string) IServiceInfra {
	if cb, ok := infraCreatoMap[name]; ok {
		return cb()
	}
	return nil
}
