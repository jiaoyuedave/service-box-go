package service_infra

type (
	// Options zookeeper模块配置
	Options struct {
		//Prefix 路径前缀，支持多层级的路径
		Prefix string `yaml:"prefix"`
		//Auth 鉴权接口，预留
		Auth string
		//Hosts 发现用地址
		Hosts []string `yaml:"hosts"`
	}

	Option func(*Options)
)

func WithHosts(hosts []string) Option {
	return func(o *Options) {
		o.Hosts = hosts
	}
}

func WithAppendHost(host string) Option {
	return func(o *Options) {
		//合法性检查，去重
		for _, h := range o.Hosts {
			if h == host {
				return
			}
		}
		o.Hosts = append(o.Hosts, host)
	}
}

func WithPrefix(prefix string) Option {
	return func(o *Options) {
		o.Prefix = prefix
	}
}

func (o *Options) PreProcess() {

	if len(o.Prefix) == 0 {
		o.Prefix = "/"
		return
	}

	if o.Prefix == "/" {
		return
	}

	// 以/开头
	if o.Prefix[0] != '/' {
		o.Prefix = "/" + o.Prefix
	}

	//移除以/ 结尾
	if o.Prefix[len(o.Prefix)-1] == '/' {
		o.Prefix = o.Prefix[:len(o.Prefix)-1]
	}
}
