package slog

import "fmt"

type Level int8

const (
	LogDebug Level = iota - 1
	LogInfo
	LogWarn
	LogError //error print trace back if config
	LogFatal //fatal print trace back and panic if develop!
)

func (l Level) String() string {
	switch l {
	case LogDebug:
		return "debug"
	case LogInfo:
		return "info"
	case LogWarn:
		return "warn"
	case LogError:
		return "error"
	case LogFatal:
		return "fatal"
	}
	return ""
}

// Enabled returns true if the given level is at or above this level.
func (l Level) Enabled(lvl Level) bool {
	return lvl >= l
}

// GetLevel converts a level string into a logger Level value.
// returns an error if the input string does not match known values.
func GetLevel(levelStr string) (Level, error) {
	switch levelStr {
	case "debug":
		return LogDebug, nil
	case "info":
		return LogInfo, nil
	case "warn":
		return LogWarn, nil
	case "error":
		return LogError, nil
	case "fatal":
		return LogFatal, nil
	}
	return LogInfo, fmt.Errorf("Unknown LogLevel String: '%s', defaulting to InfoLevel\n", levelStr)
}
