package slog

type (
	BoxLogger interface {
		// Init initialises options
		Init(options ...Option) error
		// Options the Logger options
		Options() *Options
		// Log writes a log entry
		Log(level Level, v ...interface{})
		// Logf writes a formatted log entry
		Logf(level Level, format string, v ...interface{})
		// Named will create child logger with prefix
		Named(string) BoxLogger
		// Children will return a children with new option !
		Children(...Option) BoxLogger

		Debug(string, ...interface{})
		Info(string, ...interface{})
		Warn(string, ...interface{})
		Error(string, ...interface{})
		Fatal(string, ...interface{})
	}
)

var (
	defaultLog BoxLogger
)

//SetDefaultLog will create logger by tag
func SetDefaultLog(logger BoxLogger) {
	opts := []Option{
		//WithCallerSkipCount(1),
	}
	defaultLog = logger.Children(opts...)
}

func Children(opts ...Option) BoxLogger {
	if defaultLog == nil {
		return nil
	}
	return defaultLog.Children(opts...)
}

func Debug(format string, args ...interface{}) {
	if defaultLog == nil {
		return
	}
	defaultLog.Debug(format, args...)
}

func Info(format string, args ...interface{}) {
	if defaultLog == nil {
		return
	}
	defaultLog.Info(format, args...)
}

func Warn(format string, args ...interface{}) {
	if defaultLog == nil {
		return
	}
	defaultLog.Warn(format, args...)
}

func Error(format string, args ...interface{}) {
	if defaultLog == nil {
		return
	}
	defaultLog.Error(format, args...)
}

func Fatal(format string, args ...interface{}) {
	if defaultLog == nil {
		return
	}
	defaultLog.Fatal(format, args...)
}
