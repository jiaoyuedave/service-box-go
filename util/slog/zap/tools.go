package zap

import (
	"fmt"
	"go.uber.org/zap/zapcore"
)

func LogNameEncoder(loggerName string, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString(loggerName)
}

func CapitalLevelEncoder(l zapcore.Level, enc zapcore.PrimitiveArrayEncoder) {

	level := ""

	switch l {
	case zapcore.DebugLevel:
		level = "[debug]"
	case zapcore.InfoLevel:
		level = "[info]"
	case zapcore.WarnLevel:
		level = "[warn]"
	case zapcore.ErrorLevel:
		level = "[error]"
	case zapcore.DPanicLevel:
		level = "[dpanic]"
	case zapcore.PanicLevel:
		level = "[panic]"
	case zapcore.FatalLevel:
		level = "[fatal]"
	default:
		level = fmt.Sprintf("[level(%d)]", l)
	}
	enc.AppendString(level)
}

func ShortEncodeCaller(caller zapcore.EntryCaller, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString(fmt.Sprintf("[%s]", caller.TrimmedPath()))
}
