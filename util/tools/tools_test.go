package tools

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"
)

func TestParseIp(t *testing.T) {
	cases := []struct {
		input  string
		except string
	}{
		{
			"localhost:7999",
			"127.0.0.1:7999",
		},
		{
			"hostname:7999",
			"127.0.0.1:7999",
		},
		{
			"192.168.1.1:7999",
			"192.168.1.1:7999",
		},
	}

	for idx := range cases {
		ip, err := ParseIp(cases[idx].input)
		if err != nil {
			t.Fatalf("unexception error %v", err)
			return
		}
		if ip != cases[idx].except {
			t.Errorf("unexception ip addrs %s get %s", cases[idx].except, ip)
		}
	}
}

func TestListFileInDirWithSuffix(t *testing.T) {
	path, _ := os.Getwd()
	fmt.Println(path) // for example /home/user
	files, err := ListFileInDirWithSuffix("../../example", ".json")
	if err != nil {
		t.Fatal(err)
	}

	exception := make(map[string]bool)
	exception["testcallee.idl.protobuf.json"] = true
	exception["testcaller.idl.protobuf.json"] = true

	if len(exception) != len(files) {
		t.Fatalf("not match files lens %v", files)
	}

	for _, file := range files {
		basename := filepath.Base(file)
		if _, ok := exception[basename]; !ok {
			t.Fatalf("no excepted file %s", basename)
		}
	}
}
