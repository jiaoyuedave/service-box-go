package custom

import (
	"testing"
)

func TestCustomUUID_New32UUID(t *testing.T) {
	ug := NewCustomUUID()
	ug.cfg.Seed = 233

	var result []uint32
	for i := 0; i < 100; i++ {
		result = append(result, ug.New32UUID())
	}

	checkMap := make(map[uint32]bool)
	for _, num := range result {
		if _, ok := checkMap[num]; ok {
			t.Fatalf("repeated uuid %d", num)
		}
		checkMap[num] = true
	}
}
