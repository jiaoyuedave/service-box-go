package custom

type Options struct {
	Seed uint16 `yaml:"seed"`
}

//Option is the decorators func of change options variable
type Option func(o *Options)
