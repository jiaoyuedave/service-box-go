package uuid

type (
	IUuidGenerator interface {
		New() [16]byte
		New32UUID() uint32
		New64UUID() uint64
		NewStringUUID() string
		ParseFromByte([]byte) string
	}
)
